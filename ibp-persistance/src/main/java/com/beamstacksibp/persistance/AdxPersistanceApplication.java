package com.beamstacksibp.persistance;

import org.springframework.boot.SpringApplication;

/*@SpringBootApplication*/
/**
 * @author AmkuGlory
 *
 */
public class AdxPersistanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdxPersistanceApplication.class, args);
	}
}
