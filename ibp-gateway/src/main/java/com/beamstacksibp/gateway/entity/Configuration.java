package com.beamstacksibp.gateway.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author AmkuGlory
 *
 */
@Entity
@Table(name = "IBP_CONFIGURATION")
@Data
@EqualsAndHashCode(callSuper=false)
public class Configuration extends BaseEntity{
	
	@Column(name = "attributeName")
	private String attributeName;
	
	@Type(type="org.hibernate.type.TextType")
	@Column(name="attributeValue",length=Integer.MAX_VALUE)
	@Lob
	private String attributeValue;
	
	@Column(name = "type")
	private String type;
	
}
