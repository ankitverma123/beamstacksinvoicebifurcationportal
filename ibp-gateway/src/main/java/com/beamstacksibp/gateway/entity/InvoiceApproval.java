package com.beamstacksibp.gateway.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author AmkuGlory
 *
 */
@Entity
@Table(name = "IBP_INVOICE_APPROVAL")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "id")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class InvoiceApproval extends BaseEntity {

	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "approvalNotes", length = Integer.MAX_VALUE)
	@Lob
	private String approvalNotes;

	@Column(name = "mappedInvoice")
	private UUID mappedInvoice;

	@Column(name = "mappedUser")
	private UUID mappedUser;

}
