package com.beamstacksibp.gateway.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ankitkverma
 *
 */
@Entity
@Table(name="IBP_PRIVILEGEPAGE")
@Data
@EqualsAndHashCode(callSuper=false)
public class PrivilegePageCode extends BaseEntity{
	
	@Column(name ="path")
	private String path;
	
	@Column(name ="icon")
	private String icon;
	
	@Column(name ="label")
	private String label;
	
	@Column(name ="orderId")
	private Long orderId;
	
	@Column(name ="privilegeId")
	private UUID privilegeId;

}
