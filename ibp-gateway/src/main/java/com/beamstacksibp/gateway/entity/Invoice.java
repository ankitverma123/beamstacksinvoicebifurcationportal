package com.beamstacksibp.gateway.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.beamstacksibp.gateway.entity.bank_ica_management.ICA;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ankitkverma
 *
 */
@Entity
@Table(name = "IBP_INVOICE")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "id")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Invoice extends BaseEntity {

	@Column(name = "file")
	private String file;

	@Column(name = "month")
	private String month;

	@Column(name = "year")
	private String year;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "issuer", nullable = false)
	private ICA issuer;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "acquirer", nullable = false)
	private ICA acquirer;

	@Column(name = "transFunction")
	private String transFunction;

	@Column(name = "processingCode")
	private String processingCode;

	@Column(name = "transactionCount")
	private String transactionCount;

	@Column(name = "reconAmtCcy")
	private String reconAmtCcy;

	@Column(name = "reconCurrCode")
	private String reconCurrCode;

	@Column(name = "reconAmt")
	private String reconAmt;

	@Column(name = "reconIndicator")
	private String reconIndicator;

	@Column(name = "transFeeCcy")
	private String transFeeCcy;

	@Column(name = "transFeeCurrCode")
	private String transFeeCurrCode;

	@Column(name = "transAmt")
	private String transAmt;

	@Column(name = "transAmtIndicator")
	private String transAmtIndicator;

	@Column(name = "acceptanceBrand")
	private String acceptanceBrand;

	@Column(name = "invoiceNumber")
	private String invoiceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invoiceDate")
	private Date invoiceDate;

	@Column(name = "totaInvoiceAmt")
	private Double totaInvoiceAmt;

	@Column(name = "placeOfSupply")
	private String placeOfSupply;

	@Column(name = "igst")
	private String igst;

	@Column(name = "cgst")
	private String cgst;

	@Column(name = "sgst")
	private String sgst;

	@Column(name = "totalTaxableValue")
	private Double totalTaxableValue;

	@Column(name = "totalGst")
	private Double totalGst;

	@Column(name = "hsn")
	private String hsn;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "fileTrackingId", nullable = false)
	@JsonIgnore
	private FileTracking fileTrackingId;

	@Column(name = "issuerGstIn")
	private String issuerGstIn;

	@Column(name = "acquirerGstIn")
	private String acquirerGstIn;

	@Column(name = "issuerAddress")
	private String issuerAddress;

	@Column(name = "acquirerAddress")
	private String acquirerAddress;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "workFlowState", nullable = false)
	private WorkFlowState workFlowState;

	private transient String issuerBankName;
	private transient String issuerICAIdentifier;
	private transient String acquirerBankName;
	private transient String acquirerICAIdentifier;
	private transient String workFlowStateStr;
	private transient String rejectedBy;

}
