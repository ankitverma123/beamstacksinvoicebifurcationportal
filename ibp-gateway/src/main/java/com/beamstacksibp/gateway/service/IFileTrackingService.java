package com.beamstacksibp.gateway.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.FileTracking;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface IFileTrackingService {
	/**
	 * @param request
	 * @param fileName
	 * @return FileTracking persisted object
	 */
	FileTracking saveFileTracking(HttpServletRequest request, String fileName);
}
