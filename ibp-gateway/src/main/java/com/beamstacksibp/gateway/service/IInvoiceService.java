package com.beamstacksibp.gateway.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.beamstacksibp.gateway.common.BaseException;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface IInvoiceService {
	/**
	 * @param file
	 * @param request
	 * @return List of Invoices string with their invoice number to indicate this
	 *         has been processed successfully or not.
	 * @throws BaseException
	 */
	Object importInvoices(MultipartFile file, HttpServletRequest request) throws BaseException;

	Map<String, Object> listInvoices(Integer pageNumber, Integer pageSize, String sortField, String sortDirection,
			String filter) throws BaseException;

	/**
	 * @param requestMap
	 * @return Approve or Reject specific invoice, audit log and update work flow
	 *         state
	 * @throws BaseException
	 */
	@Transactional
	Object approveRejectInvoice(Map<String, String> requestMap) throws BaseException;

	/**
	 * @param invoiceUUID
	 * @param failureReason
	 * @param rejectionNotes
	 * @throws BaseException
	 */
	void logInvoiceRejectionReason(UUID invoiceUUID, UUID failureReason, String rejectionNotes) throws BaseException;

	/**
	 * @param invoiceUUID
	 * @param approvalNotes
	 * @throws BaseException
	 */
	void logInvoiceApproval(UUID invoiceUUID, String approvalNotes) throws BaseException;

	/**
	 * @param invoice
	 * @return List of dictionaries
	 */
	List<Map<String, Object>> invoiceAuditLog(UUID invoice);
}
