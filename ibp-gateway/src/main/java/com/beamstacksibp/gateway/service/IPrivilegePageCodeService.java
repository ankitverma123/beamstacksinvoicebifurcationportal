package com.beamstacksibp.gateway.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.PrivilegePageCode;

/**
 * @author ankitkverma
 *
 */
@Service
public interface IPrivilegePageCodeService {
	public PrivilegePageCode findByPrivilegeId(UUID id);
	public List<PrivilegePageCode> findByPrivilegeId(List<UUID> id);
}
