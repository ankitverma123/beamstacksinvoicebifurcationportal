package com.beamstacksibp.gateway.service;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.WorkFlowState;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface IWorkFlowStateService {

	/**
	 * @param identifier (is unique identifier associated with each workFlowState)
	 * @return Will return the unique workFlowState object based on identifier mapped.
	 */
	WorkFlowState getWorkFlowState(String identifier);
	
}
