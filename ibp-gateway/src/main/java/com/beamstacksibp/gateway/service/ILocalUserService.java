package com.beamstacksibp.gateway.service;

import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.dto.LocalUserDTO;
import com.beamstacksibp.gateway.entity.LocalUser;

/**
 * @author ankitkverma
 *
 */
@Service
public interface ILocalUserService {
	public Map<String, Object> listUsers(Integer pageNumber, Integer pageSize, String sortField,
			String sortDirection, String filter, String fields) throws BaseException;

	public LocalUser findLocalUsers(UUID id);

	public String deleteLocalUser(UUID id);

	public Object saveUpdate(LocalUserDTO dto) throws BaseException;

	public LocalUser authenticateUser(String username, String password);

	public LocalUser authenticateUser(String username);

	public Long totalCount();

	public LocalUser findLocalUserByEmail(String email);
}
