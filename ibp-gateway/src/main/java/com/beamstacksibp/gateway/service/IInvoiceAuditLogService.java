package com.beamstacksibp.gateway.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface IInvoiceAuditLogService {
	/**
	 * @param invoiceUUID (UUID of the persisted invoice)
	 * @param workFlowState (UUID of the persisted WorkFlowState)
	 */
	void saveAuditLog(UUID invoiceUUID, UUID workFlowState);
	
	/**
	 * @param invoiceUUID (UUID of the persisted invoice)
	 * @param workFlowState (UUID of the persisted WorkFlowState)
	 * @return
	 */
	String rejectedBy(UUID invoiceUUID, UUID workFlowState);
	
	/**
	 * @param invoice
	 * @return List of dictionaries
	 */
	List<Map<String, Object>> invoiceAuditLog(UUID invoice);
}
