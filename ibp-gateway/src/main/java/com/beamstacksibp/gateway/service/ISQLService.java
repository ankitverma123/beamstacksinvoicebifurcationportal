package com.beamstacksibp.gateway.service;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface ISQLService {
	@Transactional
	String insertUpdateIca(Map<String, String> requestMap);
	
	@Transactional
	String insertUpdateBank(Map<String, String> requestMap);
}
