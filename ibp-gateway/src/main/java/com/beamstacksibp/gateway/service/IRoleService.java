package com.beamstacksibp.gateway.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.dto.RoleDTO;
import com.beamstacksibp.gateway.entity.Role;

/**
 * @author ankitkverma
 *
 */
@Service	
public interface IRoleService {
	public List<Role> findRoles();
	public Role findRoles(Long id);
	public Role findRoles(String name);
	public String deleteRole(Long id);
	public Object saveUpdate(RoleDTO dto);
	public Object assignPrivileges(Long roleId, Long[] privilegesId)throws BaseException;
	public List<Map<String, Object>> fetchRolesList();
}
