package com.beamstacksibp.gateway.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * @author AmkuGlory
 *
 */
@Service
public interface IConfigurationService {
	List<Map<String, String>> getConfigurations(String type);
	List<Map<String, String>> getExcelConfigurations(String type);
}
