package com.beamstacksibp.gateway.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.PrivilegePageCode;

/**
 * @author ankitkverma
 *
 */
@Service
public interface INavigationService {
	public List<PrivilegePageCode> getNavBar(HttpServletRequest request);
}
