package com.beamstacksibp.gateway.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.dto.PrivilegeDTO;
import com.beamstacksibp.gateway.entity.Privilege;

/**
 * @author ankitkverma
 *
 */
@Service
public interface IPrivilegeService {
	public List<Privilege> findPrivileges();
	public Privilege findPrivileges(Long id);
	public String deletePrivileges(Long id);
	public Object saveUpdate(PrivilegeDTO dto);
}
