
package com.beamstacksibp.gateway.rolemodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AdxPrivileges",
    "AdxRole",
    "AdxRolePrivileges",
    "AdxDemoUser",
    "AdxPrivilegePageCodes",
    "ChartConfiguration"
})
public class RolePrivilegeUtil {

    @JsonProperty("AdxPrivileges")
    private List<String> adxPrivileges = null;
    @JsonProperty("AdxRole")
    private List<String> adxRole = null;
    @JsonProperty("AdxRolePrivileges")
    private List<AdxRolePrivilege> adxRolePrivileges = null;
    @JsonProperty("AdxDemoUser")
    private List<AdxDemoUser> adxDemoUser = null;
    @JsonProperty("AdxPrivilegePageCodes")
    private List<AdxPrivilegePageCode> adxPrivilegePageCodes = null;
    @JsonProperty("ChartConfiguration")
    private List<ChartConfiguration> chartConfiguration = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AdxPrivileges")
    public List<String> getAdxPrivileges() {
        return adxPrivileges;
    }

    @JsonProperty("AdxPrivileges")
    public void setAdxPrivileges(List<String> adxPrivileges) {
        this.adxPrivileges = adxPrivileges;
    }

    @JsonProperty("AdxRole")
    public List<String> getAdxRole() {
        return adxRole;
    }

    @JsonProperty("AdxRole")
    public void setAdxRole(List<String> adxRole) {
        this.adxRole = adxRole;
    }

    @JsonProperty("AdxRolePrivileges")
    public List<AdxRolePrivilege> getAdxRolePrivileges() {
        return adxRolePrivileges;
    }

    @JsonProperty("AdxRolePrivileges")
    public void setAdxRolePrivileges(List<AdxRolePrivilege> adxRolePrivileges) {
        this.adxRolePrivileges = adxRolePrivileges;
    }

    @JsonProperty("AdxDemoUser")
    public List<AdxDemoUser> getAdxDemoUser() {
        return adxDemoUser;
    }

    @JsonProperty("AdxDemoUser")
    public void setAdxDemoUser(List<AdxDemoUser> adxDemoUser) {
        this.adxDemoUser = adxDemoUser;
    }

    @JsonProperty("AdxPrivilegePageCodes")
    public List<AdxPrivilegePageCode> getAdxPrivilegePageCodes() {
        return adxPrivilegePageCodes;
    }

    @JsonProperty("AdxPrivilegePageCodes")
    public void setAdxPrivilegePageCodes(List<AdxPrivilegePageCode> adxPrivilegePageCodes) {
        this.adxPrivilegePageCodes = adxPrivilegePageCodes;
    }

    @JsonProperty("ChartConfiguration")
    public List<ChartConfiguration> getChartConfiguration() {
        return chartConfiguration;
    }

    @JsonProperty("ChartConfiguration")
    public void setChartConfiguration(List<ChartConfiguration> chartConfiguration) {
        this.chartConfiguration = chartConfiguration;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
