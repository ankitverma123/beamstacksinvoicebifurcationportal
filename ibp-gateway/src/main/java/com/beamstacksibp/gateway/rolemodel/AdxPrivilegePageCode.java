
package com.beamstacksibp.gateway.rolemodel;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "privilege",
    "privilegePageCode"
})
public class AdxPrivilegePageCode {

    @JsonProperty("privilege")
    private String privilege;
    @JsonProperty("privilegePageCode")
    private PrivilegePageCode privilegePageCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("privilege")
    public String getPrivilege() {
        return privilege;
    }

    @JsonProperty("privilege")
    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @JsonProperty("privilegePageCode")
    public PrivilegePageCode getPrivilegePageCode() {
        return privilegePageCode;
    }

    @JsonProperty("privilegePageCode")
    public void setPrivilegePageCode(PrivilegePageCode privilegePageCode) {
        this.privilegePageCode = privilegePageCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
