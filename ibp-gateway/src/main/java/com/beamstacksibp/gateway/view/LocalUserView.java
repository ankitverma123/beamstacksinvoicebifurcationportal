package com.beamstacksibp.gateway.view;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author AmkuGlory
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocalUserView {
	private UUID id;
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
	private String bank;
}
