package com.beamstacksibp.gateway.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.beamstacksibp.gateway.entity.PrivilegePageCode;

/**
 * @author ankitkverma
 *
 */
public interface IPageCodeRepository extends JpaRepository<PrivilegePageCode, Long>{
	
	@Query("FROM PrivilegePageCode pageCode where pageCode.privilegeId = :id")
	PrivilegePageCode findPageCodeByPrivilegeId(@Param("id") UUID id);
	
	@Query("FROM PrivilegePageCode pageCode where pageCode.privilegeId in :id ORDER BY pageCode.orderId ASC")
	List<PrivilegePageCode> findPageCodeByPrivilegeId(@Param("id") List<UUID> id);
	
}
