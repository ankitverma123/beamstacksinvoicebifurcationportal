package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.InvoiceAuditLog;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IInvoiceAuditLogRepository extends JpaRepository<InvoiceAuditLog, Long>{

}
