package com.beamstacksibp.gateway.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.beamstacksibp.gateway.entity.Role;

/**
 * @author ankitkverma
 *
 */
public interface IRoleRepository extends JpaRepository<Role, Long>{
	
	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Role c WHERE c.name = :name")
    boolean existsByName(@Param("name") String name);
	
	@Query("FROM Role role where role.id = :id")	
	public Role findRoleById(@Param("id") Long id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM Role role where role.id = :id")
	public int deleteRoleById(@Param("id") Long id);
	
	@Query("SELECT new map(role.id as id, role.name as name) FROM Role role WHERE role.name = 'Admin' OR role.name = 'Executive'")	
	public List<Map<String, Object>> findRolesList();
	
	public Role findByName(String name);
	
}
