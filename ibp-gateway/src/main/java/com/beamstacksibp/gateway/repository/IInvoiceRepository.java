package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.Invoice;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IInvoiceRepository extends JpaRepository<Invoice, Long>{

}
