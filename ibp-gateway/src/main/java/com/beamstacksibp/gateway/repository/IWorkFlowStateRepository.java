package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.WorkFlowState;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IWorkFlowStateRepository extends JpaRepository<WorkFlowState, Long> {

	@Query("FROM WorkFlowState entityObj where entityObj.identifier = :identifier")
	WorkFlowState findWorkFlowStateByIdentifier(@Param("identifier") String identifier);

}
