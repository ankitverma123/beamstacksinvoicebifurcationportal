package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.InvoiceRejections;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IInvoiceRejectionRepository extends JpaRepository<InvoiceRejections, Long>{

}
