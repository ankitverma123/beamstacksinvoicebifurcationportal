package com.beamstacksibp.gateway.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.beamstacksibp.gateway.entity.Privilege;

/**
 * @author ankitkverma
 *
 */
public interface IPrivilegeRepository extends JpaRepository<Privilege, Long>{
	
	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Privilege c WHERE c.name = :name")
    boolean existsByName(@Param("name") String name);
	
	@Query("FROM Privilege privilege where privilege.id = :id")	
	public Privilege findPrivilegeById(@Param("id") Long id);
	
	@Query("FROM Privilege privilege where privilege.name = :name")	
	public Privilege findPrivilegeByName(@Param("name") String name);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM Privilege privilege where privilege.id = :id")
	public int deletePrivilegeById(@Param("id") Long id);
	
	@Query("FROM Privilege privilege where privilege.name in :name")	
	public List<Privilege> findPrivilegeByName(@Param("name") List<String> name);
}
