package com.beamstacksibp.gateway.repository;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.beamstacksibp.gateway.entity.LocalUser;

/**
 * @author ankitkverma
 *
 */
public interface ILocalUserRepository extends JpaRepository<LocalUser, Long> {
	
	@Query("FROM LocalUser localUser where localUser.id = :id")	
	public LocalUser findLocalUserById(@Param("id") UUID id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("DELETE FROM LocalUser localUser where localUser.id = :id")
	public int deleteLocalUserById(@Param("id") UUID id);
	
	@Query("FROM LocalUser localUser where localUser.userName = :username and localUser.password = :password")
	public LocalUser findLocalUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
	
	@Transactional
	@Query("FROM LocalUser localUser where localUser.userName = :username")
	public LocalUser findLocalUserByUsername(@Param("username") String username);
	
	@Transactional
	@Query("FROM LocalUser localUser where localUser.email = :email")
	public LocalUser findLocalUserByEmail(@Param("email") String email);
	
}
