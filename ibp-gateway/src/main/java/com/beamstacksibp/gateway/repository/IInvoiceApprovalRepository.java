package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.InvoiceApproval;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IInvoiceApprovalRepository extends JpaRepository<InvoiceApproval, Long>{

}
