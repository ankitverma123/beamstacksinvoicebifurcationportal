package com.beamstacksibp.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beamstacksibp.gateway.entity.FileTracking;

/**
 * @author AmkuGlory
 *
 */
@Repository
public interface IFileTrackingRepository extends JpaRepository<FileTracking, Long>{

}
