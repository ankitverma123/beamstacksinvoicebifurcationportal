package com.beamstacksibp.gateway.dto;

import java.util.UUID;

/**
 * @author AmkuGlory
 *
 */
public class LoggedInUserInstance {
	public static UUID id;
	public static String username;
	public static String email;
	public static String role;

	@SuppressWarnings("static-access")
	public LoggedInUserInstance(UUID id, String username, String email, String role) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.role = role;
	}
}
