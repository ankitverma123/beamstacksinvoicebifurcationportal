package com.beamstacksibp.gateway.dto;

import java.util.UUID;

import lombok.Data;

/**
 * @author ankitkverma
 *
 */
@Data
public class LocalUserDTO {
	private UUID id;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private Long roleId;
	private String email;
}
