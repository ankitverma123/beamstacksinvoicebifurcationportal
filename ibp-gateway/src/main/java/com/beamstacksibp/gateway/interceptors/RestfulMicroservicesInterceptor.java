package com.beamstacksibp.gateway.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.beamstacksibp.gateway.dto.LoggedInUserInstance;
import com.beamstacksibp.gateway.security.JwtUser;

/**
 * @author AmkuGlory
 *
 */
@Component
public class RestfulMicroservicesInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
			JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			LoggedInUserInstance.id = user.getId();
			LoggedInUserInstance.username = user.getUsername();
			LoggedInUserInstance.email = user.getEmail();
			String roles = "";
			for (GrantedAuthority ga : user.getAuthorities()) {
				roles += String.format("%s,", ga.getAuthority());
			}
			LoggedInUserInstance.role = roles;
			System.out.println("\n======================================================================");
			System.out.println(
					String.format("loggedInUserId= %s, loggedInUsername= %s, loggedInEmail= %s, loggedInRole= %s",
							LoggedInUserInstance.id, LoggedInUserInstance.username, LoggedInUserInstance.email,
							LoggedInUserInstance.role));
			System.out.println("======================================================================\n");
		}
		return true;
	}

}
