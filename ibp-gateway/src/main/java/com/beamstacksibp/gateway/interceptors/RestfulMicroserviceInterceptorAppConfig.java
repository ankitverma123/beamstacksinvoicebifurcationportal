package com.beamstacksibp.gateway.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author ancsingla
 *
 */
@Component
public class RestfulMicroserviceInterceptorAppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	RestfulMicroservicesInterceptor microservicesInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(microservicesInterceptor);
	}
}
