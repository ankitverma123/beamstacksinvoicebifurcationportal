package com.beamstacksibp.gateway.impl;

import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.common.Utils;
import com.beamstacksibp.gateway.entity.bank_ica_management.Bank;
import com.beamstacksibp.gateway.entity.bank_ica_management.ICA;
import com.beamstacksibp.gateway.service.ISQLService;

/**
 * @author AmkuGlory
 *
 */
@Service
public class SQLServiceImpl implements ISQLService {

	@Autowired
	private EntityManager entityManager;

	@Override
	@Transactional
	public String insertUpdateIca(Map<String, String> requestMap) {
		String identifier = requestMap.get("identifier");
		String icaIdStr = requestMap.get("id");
		UUID bank = UUID.fromString(requestMap.get("bank"));
		UUID icaId = Utils.checkIfUUID(icaIdStr) ? UUID.fromString(icaIdStr) : null;
		if (icaId == null) {
			boolean alreadyExist = exisitngICACheck(identifier, null);
			if (alreadyExist) {
				return identifier + "Already Exist";
			}

			Bank persistedBank = getBank(bank);

			ICA ica = new ICA();
			ica.setIdentifier(identifier);
			ica.setBank(persistedBank);
			entityManager.persist(ica);

			return "New ICA created succsfully for " + persistedBank.getName();
		} else {
			boolean alreadyExist = exisitngICACheck(identifier, null);
			if (alreadyExist) {
				return identifier + "Already Exist";
			}

			String query = "UPDATE ICA ica SET ica.identifier = :identifier WHERE ica.id= :icaID AND ica.bank.id = :bankID";
			int recordUpdated = entityManager.createQuery(query).setParameter("identifier", identifier)
					.setParameter("icaID", icaId).setParameter("bankID", bank).executeUpdate();
			return recordUpdated > 0 ? "ICA updated succssfully" : "Failed to update ica identifier " + identifier;
		}
	}

	private boolean exisitngICACheck(String identifier, UUID id) {
		String query = "SELECT CASE WHEN (COUNT(ica) > 0) THEN true ELSE false END ";
		if (id != null) {
			query += "FROM ICA ica WHERE ica.identifier = :identifier AND ica.id <> :icaId";
			query += "AND ica.id <> :icaId";
			Query queryObj = entityManager.createQuery(query).setParameter("identifier", identifier)
					.setParameter("icaId", id);
			return (boolean) queryObj.getSingleResult();
		} else {
			query += "FROM ICA ica WHERE ica.identifier = :identifier ";
			Query queryObj = entityManager.createQuery(query).setParameter("identifier", identifier);
			return (boolean) queryObj.getSingleResult();
		}
	}

	private Bank getBank(UUID bank) {
		String query = "FROM Bank bank WHERE bank.id = :bankID";
		return (Bank) entityManager.createQuery(query).setParameter("bankID", bank).getSingleResult();
	}

	@Override
	@Transactional
	public String insertUpdateBank(Map<String, String> requestMap) {
		String bankName = requestMap.get("name");
		String bankIdStr = requestMap.get("id");
		UUID bankId = Utils.checkIfUUID(bankIdStr) ? UUID.fromString(bankIdStr) : null;
		if (bankId == null) {
			if (!existingBank(bankName, null)) {
				Bank bank = new Bank();
				bank.setName(bankName);
				entityManager.persist(bank);
				return bankName + " created successfully";
			} else {
				return "Bank name already exist, Needs to be unique";
			}
		} else {
			if (!existingBank(bankName, bankId)) {
				String query = "UPDATE Bank bank SET bank.name = :bankName WHERE bank.id= :bankId";
				int updated = entityManager.createQuery(query).setParameter("bankName", bankName)
						.setParameter("bankId", bankId).executeUpdate();
				return updated > 0 ? "Bank updated successfully" : "Failed to updated bank";
			} else {
				return "Bank name already exist";
			}
		}
	}

	private boolean existingBank(String bankName, UUID id) {
		String query = "SELECT CASE WHEN (COUNT(bank) > 0) THEN true ELSE false END ";
		if (id != null) {
			query += "FROM Bank bank WHERE bank.name = :bankName AND bank.id <> :bankId";
			Query sqlQuery = entityManager.createQuery(query);
			sqlQuery.setParameter("bankName", bankName);
			sqlQuery.setParameter("bankId", id);
			return (boolean) sqlQuery.getSingleResult();
		} else {
			query += "FROM Bank bank WHERE bank.name = :bankName";
			Query sqlQuery = entityManager.createQuery(query);
			sqlQuery.setParameter("bankName", bankName);
			return (boolean) sqlQuery.getSingleResult();
		}
	}

}
