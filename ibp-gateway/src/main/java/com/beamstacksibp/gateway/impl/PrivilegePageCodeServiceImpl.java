package com.beamstacksibp.gateway.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.PrivilegePageCode;
import com.beamstacksibp.gateway.repository.IPageCodeRepository;
import com.beamstacksibp.gateway.service.IPrivilegePageCodeService;

/**
 * @author ankitkverma
 *
 */
@Service
public class PrivilegePageCodeServiceImpl implements IPrivilegePageCodeService{

	@Autowired
	private IPageCodeRepository pageCodeRepo;
	
	@Override
	public PrivilegePageCode findByPrivilegeId(UUID id) {
		// TODO Auto-generated method stub
		return pageCodeRepo.findPageCodeByPrivilegeId(id);
	}

	@Override
	public List<PrivilegePageCode> findByPrivilegeId(List<UUID> id) {
		// TODO Auto-generated method stub
		return pageCodeRepo.findPageCodeByPrivilegeId(id);
	}

}
