package com.beamstacksibp.gateway.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.Privilege;
import com.beamstacksibp.gateway.entity.PrivilegePageCode;
import com.beamstacksibp.gateway.entity.Role;
import com.beamstacksibp.gateway.security.JwtTokenUtil;
import com.beamstacksibp.gateway.service.INavigationService;
import com.beamstacksibp.gateway.service.IPrivilegePageCodeService;
import com.beamstacksibp.gateway.service.IRoleService;

/**
 * @author ankitkverma
 *
 */
@Service
public class NavigationServiceImpl implements INavigationService{
	
	@Value("${jwt.header}")
    private String tokenHeader;
	
	@Autowired
    private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private IRoleService roleService;
	
	@Autowired
	private IPrivilegePageCodeService pageCodeService;
	
	@Override
	public List<PrivilegePageCode> getNavBar(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        
        List<UUID> privilegeIds = new ArrayList<>();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for(GrantedAuthority ga : authentication.getAuthorities()) {
        	String Authority = ga.getAuthority();
        	Role role = roleService.findRoles(Authority);
        	Collection<Privilege> privileges = role.getPrivileges();
        	
        	for(Privilege prv : privileges) {
        		privilegeIds.add(prv.getId());
        	}
        }
        
        List<PrivilegePageCode> pageCodes = pageCodeService.findByPrivilegeId(privilegeIds);
        
        return pageCodes;
	}

}
