package com.beamstacksibp.gateway.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.FileTracking;
import com.beamstacksibp.gateway.entity.LocalUser;
import com.beamstacksibp.gateway.repository.IFileTrackingRepository;
import com.beamstacksibp.gateway.security.JwtTokenUtil;
import com.beamstacksibp.gateway.service.IFileTrackingService;

/**
 * @author AmkuGlory
 *
 */
@Service
public class FileTrackingServiceImpl implements IFileTrackingService{
	
	@Value("${jwt.header}")
    private String tokenHeader;
	
	@Autowired
    private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private IFileTrackingRepository fileTrackingRepo;
	
	@Override
	public FileTracking saveFileTracking(HttpServletRequest request, String fileName) {
		String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        String localUserQuery = "FROM LocalUser user where user.userName='"+username+"'";
        LocalUser loggedInUser = (LocalUser) entityManager.createQuery(localUserQuery).getSingleResult();
        
        FileTracking fileTracking = new FileTracking();
        fileTracking.setName(fileName);
        fileTracking.setCreatedAt(new Date());
        fileTracking.setUploadedBy(loggedInUser);
        return fileTrackingRepo.save(fileTracking);
	}

}
