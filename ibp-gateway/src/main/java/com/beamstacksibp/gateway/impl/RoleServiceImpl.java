package com.beamstacksibp.gateway.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.ResponseData;
import com.beamstacksibp.gateway.dto.RoleDTO;
import com.beamstacksibp.gateway.entity.Privilege;
import com.beamstacksibp.gateway.entity.Role;
import com.beamstacksibp.gateway.repository.IPrivilegeRepository;
import com.beamstacksibp.gateway.repository.IRoleRepository;
import com.beamstacksibp.gateway.service.IRoleService;

/**
 * @author ankitkverma
 *
 */
@Service
public class RoleServiceImpl implements IRoleService{

	@Autowired
	private IRoleRepository roleRepository;
	
	@Autowired
	private IPrivilegeRepository  privilegeRepository;
	
	@Override
	public List<Role> findRoles() {
		return roleRepository.findAll();
	}

	@Override
	public Role findRoles(Long id) {
		return roleRepository.findRoleById(id);
	}

	@Override
	public String deleteRole(Long id) {
		int deleteCount = roleRepository.deleteRoleById(id);
		return deleteCount > 0 ? "Role deleted successfully." : "Failed to delete role.";
	}

	@Override
	public Object saveUpdate(RoleDTO dto) {
		Role role = null;
		if(dto.getId()!=null)
			role = roleRepository.findRoleById(dto.getId());
		
		if(dto.getId()==null && dto.getName()!=null)
			role = roleRepository.findByName(dto.getName());
		
		role = role==null?role=new Role():role;
		role.setName(dto.getName());
		
		role = roleRepository.save(role);
		return role;
	}

	@Override
	public Object assignPrivileges(Long roleId, Long[] privilegesId) throws BaseException {
		ResponseData rd = null;
		Role role = roleRepository.findRoleById(roleId);
		if(role!=null)
		{
			Collection<Privilege> privilegesToBeApplied = role.getPrivileges();
			privilegesToBeApplied = privilegesToBeApplied.size()==0?new ArrayList<Privilege>():privilegesToBeApplied;
			for(Long privilegeId: privilegesId) {
				privilegesToBeApplied.add(privilegeRepository.findPrivilegeById(privilegeId));
			}
			
			role.setPrivileges(privilegesToBeApplied);
			role = roleRepository.save(role);
			return role;
		}else {
			throw new BaseException(RoleServiceImpl.class, "Invalid Role specified.");
		}
	}

	@Override
	public Role findRoles(String name) {
		return roleRepository.findByName(name);
	}

	@Override
	public List<Map<String, Object>> fetchRolesList() {
		return roleRepository.findRolesList();
	}
	
}
