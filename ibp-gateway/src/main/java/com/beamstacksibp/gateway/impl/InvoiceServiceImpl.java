package com.beamstacksibp.gateway.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.CommonConstants;
import com.beamstacksibp.gateway.common.Filter;
import com.beamstacksibp.gateway.common.FiltersPredicateUtil;
import com.beamstacksibp.gateway.common.Utils;
import com.beamstacksibp.gateway.controller.InvoiceController;
import com.beamstacksibp.gateway.dto.LoggedInUserInstance;
import com.beamstacksibp.gateway.entity.FileTracking;
import com.beamstacksibp.gateway.entity.Invoice;
import com.beamstacksibp.gateway.entity.InvoiceApproval;
import com.beamstacksibp.gateway.entity.InvoiceRejections;
import com.beamstacksibp.gateway.entity.WorkFlowState;
import com.beamstacksibp.gateway.entity.bank_ica_management.ICA;
import com.beamstacksibp.gateway.repository.IInvoiceApprovalRepository;
import com.beamstacksibp.gateway.repository.IInvoiceRejectionRepository;
import com.beamstacksibp.gateway.repository.IInvoiceRepository;
import com.beamstacksibp.gateway.service.IConfigurationService;
import com.beamstacksibp.gateway.service.IFileTrackingService;
import com.beamstacksibp.gateway.service.IInvoiceAuditLogService;
import com.beamstacksibp.gateway.service.IInvoiceService;
import com.beamstacksibp.gateway.service.IWorkFlowStateService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.monitorjbl.xlsx.StreamingReader;

/**
 * @author AmkuGlory
 *
 */
@Service
public class InvoiceServiceImpl implements IInvoiceService {

	@Autowired
	private IConfigurationService configService;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private IInvoiceRepository invoiceRepository;

	@Autowired
	private IFileTrackingService fileTrackingService;

	@Autowired
	private IInvoiceAuditLogService invoiceAuditLogService;

	@Autowired
	private IInvoiceRejectionRepository invoiceRejectionRepo;

	@Autowired
	private IInvoiceApprovalRepository invoiceApprovalRepo;

	@Autowired
	private IWorkFlowStateService workFlowStateService;

	@Override
	public Object importInvoices(MultipartFile file, HttpServletRequest request) throws BaseException {
		try {

			final String label = UUID.randomUUID().toString() + ".xlsx";
			final String filepath = "/tmp/" + label;
			byte[] bytes = file.getBytes();
			File fh = new File("/tmp/");
			if (!fh.exists()) {
				fh.mkdir();
			}

			FileOutputStream writer = new FileOutputStream(filepath);
			writer.write(bytes);
			writer.close();

			FileTracking fileTrackObject = fileTrackingService.saveFileTracking(request, filepath);

			/*
			 * We need to load the INVOICE EXCEL TEMPLATE which we have configured in
			 * Configuration entity.
			 */
			List<Map<String, String>> invoiceExcelTemplate = configService
					.getExcelConfigurations(CommonConstants.INVOICE_EXCEL_TEMPLATE);
			Map<String, String> dictionary = convertToListOfDictionaries(invoiceExcelTemplate);

			try (InputStream is = new FileInputStream(filepath);
					Workbook workbook = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(is)) {

				for (Sheet sheet : workbook) {
					int rowNumber = 0;
					Row headerRow = null;
					List<Map<String, Object>> excelSheetDataHolder = new ArrayList<>();
					int numberOfColumns = 0;
					for (Row row : sheet) {
						if (rowNumber == 0) {
							headerRow = row;
							numberOfColumns = headerRow.getPhysicalNumberOfCells();
						} else {
							if (Utils.isRowEmpty(row))
								continue;
							Map<String, Object> rowData = new HashMap<>();
							for (int columnIndex = 1; columnIndex <= numberOfColumns; columnIndex++) {
								Cell c = row.getCell(columnIndex);
								String columnName = null;
								String columnValue = null;
								try {
									/*
									 * First We need to fetch the column header name If That column header name is
									 * there then we will have that otherwise NullPointerException will be thrown
									 * and it indicates that Last column has been processed.
									 */
									columnName = headerRow.getCell(columnIndex).getStringCellValue().trim();

									try {
										columnValue = c.getStringCellValue();
									} catch (NullPointerException e) {
										columnValue = null;
									}

									rowData.put(dictionary.get(columnName),
											columnValue != null ? columnValue.trim() : null);

								} catch (NullPointerException e) {
									/* System has reached at the last column level */
								}

							}
							excelSheetDataHolder.add(rowData);
						}
						rowNumber++;
					}
					/*
					 * At this point we have all the data collected from the sheet, so now at this
					 * point We need to massage the data before processing it further like
					 * persisting into DB or Audit Log
					 */
					return populateInvoices(excelSheetDataHolder, fileTrackObject);
				}

			}

		} catch (Exception e) {
			throw new BaseException(InvoiceController.class, "importInvoiceExcel() >> " + e.getMessage());
		}
		return null;
	}

	private List<Map<String, Object>> populateInvoices(List<Map<String, Object>> excelSheetDataHolder,
			FileTracking fileTrackObj) {
		List<Map<String, Object>> response = new ArrayList<>();
		for (Map<String, Object> excelRowData : excelSheetDataHolder) {
			Map<String, Object> resultSet = populateInvoices(excelRowData, fileTrackObj);
			response.add(resultSet);
		}
		return response;
	}

	public static <T> T convert(Map<String, Object> aMap, Class<T> t, ObjectMapper objectMapper) {
		try {
			return objectMapper.convertValue(aMap, objectMapper.getTypeFactory().constructType(t));
		} catch (Exception e) {
			System.out.println("Exception= " + e.getMessage());
		}
		return null;
	}

	/**
	 * @param invoice
	 * @param workFlowStateIdentifier (The specific identifier will be mapped with
	 *                                the invoice)
	 */
	private void updateWorkFlowState(Invoice invoice, String workFlowStateIdentifier) {
		invoice.setWorkFlowState(workFlowStateService.getWorkFlowState(workFlowStateIdentifier));
	}

	private Map<String, Object> populateInvoices(Map<String, Object> excelRowData, FileTracking fileTrackObj) {
		Map<String, Object> resultSet = new HashMap<>();
		ICA issuer = populateIssuer(excelRowData);
		ICA acquirer = populateAcquirer(excelRowData);
		convertStringToDate(excelRowData);
		Invoice invoice = convert(excelRowData, Invoice.class, new ObjectMapper());
		if (invoice == null) {
			resultSet.put("message", String.format("Failed to convert row with #{%s} invoice number",
					excelRowData.get("invoiceNumber")));
			resultSet.put("data", excelRowData);
		} else {
			invoice.setIssuer(issuer);
			invoice.setAcquirer(acquirer);
			invoice.setFileTrackingId(fileTrackObj);
			updateWorkFlowState(invoice, CommonConstants.WORK_FLOW_STATE_SENT_BY_AGENT);
			invoice = invoiceRepository.save(invoice);
			invoiceAuditLogService.saveAuditLog(invoice.getId(), invoice.getWorkFlowState().getId());
			resultSet.put("message",
					String.format("Invoice <b>#<i>%s</i></b> processed sucessfully.", invoice.getInvoiceNumber()));
		}
		return resultSet;
	}

	private void convertStringToDate(Map<String, Object> excelRowData) {
		String field = "invoiceDate";
		if (excelRowData.containsKey(field) && excelRowData.get(field) != null
				&& !excelRowData.get(field).toString().isEmpty()) {
			String date = excelRowData.get(field).toString();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			try {
				Date dateObj = formatter.parse(date);
				excelRowData.put(field, dateObj);
			} catch (ParseException e) {
				excelRowData.put(field, null);
			}
		} else {
			excelRowData.put(field, null);
		}
	}

	private ICA populateIssuer(Map<String, Object> excelRowData) {
		return populateIssuerAndAcquirer(excelRowData, "issuerICA", "issuerName", "issuer");
	}

	private ICA populateAcquirer(Map<String, Object> excelRowData) {
		return populateIssuerAndAcquirer(excelRowData, "acquirerICA", "acquirerName", "acquirer");
	}

	@SuppressWarnings("unchecked")
	private ICA populateIssuerAndAcquirer(Map<String, Object> excelRowData, String ica, String icaName,
			String icaToPopulate) {

		excelRowData.put(icaToPopulate, null);

		if (Utils.collectionKeyFieldChecker(excelRowData, ica)) {
			String _ica = excelRowData.get(ica).toString();
			String icaQuery = "FROM ICA ica where ica.identifier= '" + _ica + "'";

			if (Utils.collectionKeyFieldChecker(excelRowData, icaName)) {
				String bankName = excelRowData.get(icaName).toString();
				icaQuery += " AND ica.bank.name='" + bankName + "'";
			}

			List<ICA> ICAs = entityManager.createQuery(icaQuery).getResultList();
			if (!ICAs.isEmpty()) {
				excelRowData.remove(ica);
				excelRowData.remove(icaName);
				ICA icaFound = ICAs.get(0);
				return icaFound;
			}
		}
		if (excelRowData.containsKey(ica))
			excelRowData.remove(ica);
		if (excelRowData.containsKey(icaName))
			excelRowData.remove(icaName);
		return null;
	}

	private Map<String, String> convertToListOfDictionaries(List<Map<String, String>> invoiceTemplateConfiguration) {
		Map<String, String> result = new HashedMap<>();
		for (Map<String, String> map : invoiceTemplateConfiguration) {
			result.put(map.get("headerName"), map.get("fieldName"));
		}
		return result;
	}

	private List<Filter> formatFilters(String filter) {
		List<Filter> filterHolder = new ArrayList<>();
		String[] variousFilters = filter.split(",");
		for (String filterObject : variousFilters) {
			String comparator = filterObject.contains(CommonConstants.EQUAL_COMPARATOR)
					? CommonConstants.EQUAL_COMPARATOR
					: filterObject.contains(CommonConstants.CONTAINS_COMPARATOR) ? CommonConstants.CONTAINS_COMPARATOR
							: CommonConstants.IN_COMPARATOR;
			String[] filters = filterObject.split(comparator);
			String filterName = filters[0];
			String filterValue = filters[1];
			if (filterValue.equals(CommonConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID)) {
				filterHolder.add(new Filter(filterName, comparator, LoggedInUserInstance.id));
			} else if (filterValue.equals(CommonConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK)) {
				filterHolder.add(new Filter(filterName, comparator, myAllocatedBankFilterParameter()));
			} else if (filterValue.equals(CommonConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA)) {
				filterHolder.add(new Filter(filterName, comparator, myAllocatedIcaFilterParameter()));
			} else {
				filterHolder.add(new Filter(filterName, comparator, filterValue));
			}
		}
		return filterHolder;
	}

	private UUID myAllocatedBankFilterParameter() {
		String query = "SELECT user.bank.id from LocalUser user where user.id='" + LoggedInUserInstance.id + "'";
		UUID bankUUID = (UUID) entityManager.createQuery(query).getSingleResult();
		return bankUUID;
	}

	@SuppressWarnings("unchecked")
	private List<UUID> myAllocatedIcaFilterParameter() {
		String query = "SELECT allocatedIca.ica.id FROM AllocatedICA allocatedIca WHERE allocatedIca.userAllocated.id='"
				+ LoggedInUserInstance.id + "'";
		List<UUID> allocatedICA = (List<UUID>) entityManager.createQuery(query).getResultList();
		return allocatedICA;
	}

	@Override
	public Map<String, Object> listInvoices(Integer pageNumber, Integer pageSize, String sortField,
			String sortDirection, String filter) throws BaseException {
		List<Filter> filters = filter == null ? null : formatFilters(filter);
		List<Predicate> predicates = new ArrayList<>();
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Invoice> query = builder.createQuery(Invoice.class);
		Root<Invoice> r = query.from(Invoice.class);

		List<Predicate> predicatesGenerated = FiltersPredicateUtil.generatePredicatesFilters(builder, r, filters);

		if (predicatesGenerated != null && !predicatesGenerated.isEmpty())
			predicates.addAll(predicatesGenerated);

		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

		if (sortField != null && sortDirection != null) {
			if (sortDirection.equals(CommonConstants.SORT_DIRECTION_ASC)) {
				query.orderBy(builder.asc(r.get(sortField)));
			} else if (sortDirection.equals(CommonConstants.SORT_DIRECTION_DESC)) {
				query.orderBy(builder.desc(r.get(sortField)));
			}
		} else {
			query.orderBy(builder.desc(r.get(CommonConstants.ID)));
		}

		List<Invoice> result = entityManager.createQuery(query).setFirstResult((pageNumber - 1) * pageSize)
				.setMaxResults(pageSize).getResultList();
		for (Invoice invoice : result) {
			ICA issuerICA = invoice.getIssuer();
			if (issuerICA.getBank() != null) {
				invoice.setIssuerBankName(issuerICA.getBank().getName());
				invoice.setIssuerICAIdentifier(issuerICA.getIdentifier());
			}

			ICA acquirerICA = invoice.getAcquirer();
			if (acquirerICA.getBank() != null) {
				invoice.setAcquirerBankName(acquirerICA.getBank().getName());
				invoice.setAcquirerICAIdentifier(acquirerICA.getIdentifier());
			}

			invoice.setRejectedBy(
					invoiceAuditLogService.rejectedBy(invoice.getId(), invoice.getWorkFlowState().getId()));

			invoice.setWorkFlowStateStr(invoice.getWorkFlowState().getName());
		}

		int totalCount = entityManager.createQuery(query).getResultList().size();

		Map<String, Object> dataToBeReturned = new HashMap<>();
		dataToBeReturned.put(CommonConstants.RESULTS, result);
		dataToBeReturned.put(CommonConstants.TOTAL_COUNT, totalCount);

		int lastPage = 0;
		if (totalCount % pageSize == 0) {
			lastPage = totalCount / pageSize;
		} else {
			lastPage = ((totalCount / pageSize) + 1);
		}
		dataToBeReturned.put(CommonConstants.LAST_PAGE, lastPage);

		return dataToBeReturned;
	}

	@Override
	@Transactional
	public Object approveRejectInvoice(Map<String, String> requestMap) throws BaseException {
		UUID invoiceUUID = UUID.fromString(requestMap.get("invoice"));
		String _workFlowState = requestMap.get("workFloWState");

		WorkFlowState workFlowState = workFlowStateService.getWorkFlowState(_workFlowState);
		
		String updateInvoice = "UPDATE Invoice invoice SET invoice.workFlowState = :workFlowStateToUpdate WHERE invoice.id = :invoiceToBeUpdate";
		entityManager.createQuery(updateInvoice).setParameter("workFlowStateToUpdate", workFlowState)
				.setParameter("invoiceToBeUpdate", invoiceUUID).executeUpdate();

		if (requestMap.containsKey("rejectReason") && requestMap.containsKey("rejectionNote")) {
			logInvoiceRejectionReason(invoiceUUID, UUID.fromString(requestMap.get("rejectReason")),
					requestMap.get("rejectionNote"));
		}

		if (requestMap.containsKey("approvalNotes")) {
			logInvoiceApproval(invoiceUUID, requestMap.get("approvalNotes"));
		}

		invoiceAuditLogService.saveAuditLog(invoiceUUID, workFlowState.getId());

		return "workFlowState Updated Successfully";
	}

	@Override
	public void logInvoiceRejectionReason(UUID invoiceUUID, UUID failureReason, String rejectionNotes)
			throws BaseException {
		InvoiceRejections ir = new InvoiceRejections();
		ir.setMappedInvoice(invoiceUUID);
		ir.setReasonMsg(rejectionNotes);
		ir.setRejectionReason(failureReason);
		ir.setUserMapped(LoggedInUserInstance.id);
		invoiceRejectionRepo.save(ir);
	}

	@Override
	public void logInvoiceApproval(UUID invoiceUUID, String approvalNotes) throws BaseException {
		InvoiceApproval ia = new InvoiceApproval();
		ia.setMappedInvoice(invoiceUUID);
		ia.setMappedUser(LoggedInUserInstance.id);
		ia.setApprovalNotes(approvalNotes);
		invoiceApprovalRepo.save(ia);
	}

	@Override
	public List<Map<String, Object>> invoiceAuditLog(UUID invoice) {
		return invoiceAuditLogService.invoiceAuditLog(invoice);
	}

}
