package com.beamstacksibp.gateway.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.CommonConstants;
import com.beamstacksibp.gateway.common.Filter;
import com.beamstacksibp.gateway.common.FiltersPredicateUtil;
import com.beamstacksibp.gateway.dto.LocalUserDTO;
import com.beamstacksibp.gateway.dto.LoggedInUserInstance;
import com.beamstacksibp.gateway.entity.LocalUser;
import com.beamstacksibp.gateway.entity.Role;
import com.beamstacksibp.gateway.repository.ILocalUserRepository;
import com.beamstacksibp.gateway.repository.IRoleRepository;
import com.beamstacksibp.gateway.service.ILocalUserService;

/**
 * @author ankitkverma
 *
 */
@Service
public class LocalUserServiceImpl implements ILocalUserService {

	@Autowired
	private ILocalUserRepository localUserRepository;

	@Autowired
	private IRoleRepository roleRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LocalUser findLocalUsers(UUID id) {
		LocalUser localUser = localUserRepository.findLocalUserById(id);
		return localUser;
	}

	@Override
	public String deleteLocalUser(UUID id) {
		int recordDeleted = localUserRepository.deleteLocalUserById(id);
		return recordDeleted > 0 ? "User deleted successfully." : "Failed to delete user.";
	}

	@Override
	public Object saveUpdate(LocalUserDTO dto) throws BaseException {
		LocalUser user = null;
		if (dto != null && (!dto.getEmail().isEmpty() && dto.getEmail() != null)
				&& (!dto.getFirstName().isEmpty() && dto.getFirstName() != null)
				&& (!dto.getLastName().isEmpty() && dto.getLastName() != null)) {

			if (dto.getId() != null) {
				/*
				 * This case will handle the modify local user scenario
				 */
				user = localUserRepository.findLocalUserById(dto.getId());
				if (user == null) {
					String exception[] = new String[] {
							"Invalid user modify request, user with " + dto.getId() + " doesn't exists" };
					throw new BaseException(LocalUserServiceImpl.class, exception);
				} else {
					requestedUserEmailAndUsernameVerifier(dto, user);
				}
			}

			/*
			 * Fresh/New User Creation Request
			 */
			if (dto.getId() == null) {
				requestedUserEmailAndUsernameVerifier(dto, null);
			}

			user = user != null ? user : new LocalUser();

			user.setFirstName(dto.getFirstName());
			user.setLastName(dto.getLastName());
			user.setUserName(dto.getEmail());
			user.setPassword("Dummy#123");
			user.setEmail(dto.getEmail());

			Role role = roleRepository.findRoleById(dto.getRoleId());
			ArrayList<Role> roleCollection = new ArrayList<Role>();
			roleCollection.add(role);
			user.setRoles(roleCollection);

			user = localUserRepository.save(user);

			return user;
		} else {
			throw new BaseException(LocalUserServiceImpl.class,
					"Required parameters (Firstname, lastname, Email) missing or empty values.");
		}
	}

	@Override
	public LocalUser authenticateUser(String username, String password) {
		return localUserRepository.findLocalUserByUsernameAndPassword(username, password);
	}

	@Override
	public LocalUser authenticateUser(String username) {
		return localUserRepository.findLocalUserByUsername(username);
	}

	@Override
	public Long totalCount() {
		Long count = localUserRepository.count();
		return count;
	}

	@Override
	public LocalUser findLocalUserByEmail(String email) {
		return localUserRepository.findLocalUserByEmail(email);
	}

	private void requestedUserEmailAndUsernameVerifier(LocalUserDTO dto, LocalUser localUser) throws BaseException {
		if (localUser != null && !(localUser.getEmail().equals(dto.getEmail()))) {
			requestedEmailVerifier(dto.getEmail());
		}

		if (localUser == null) {
			requestedEmailVerifier(dto.getEmail());
		}
	}

	private void requestedEmailVerifier(String email) throws BaseException {
		LocalUser user = localUserRepository.findLocalUserByEmail(email);
		if (user != null) {
			/*
			 * Throw Exception if user with requested email already exists
			 */
			String exception[] = new String[] { "User with same email address already exists" };
			throw new BaseException(LocalUserServiceImpl.class, exception);
		}
	}

	@SuppressWarnings("unused")
	private void requestedUsernameVerifier(String userName) throws BaseException {
		LocalUser user = localUserRepository.findLocalUserByUsername(userName);
		if (user != null) {
			/*
			 * If user with this username already exist then throw exception
			 */
			String exception[] = new String[] { "User with same username already exists" };
			throw new BaseException(LocalUserServiceImpl.class, exception);
		}
	}

	private List<Filter> formatFilters(String filter) {
		List<Filter> filterHolder = new ArrayList<>();
		String[] variousFilters = filter.split(",");
		for (String filterObject : variousFilters) {
			String comparator = filterObject.contains(CommonConstants.EQUAL_COMPARATOR)
					? CommonConstants.EQUAL_COMPARATOR
					: filterObject.contains(CommonConstants.CONTAINS_COMPARATOR) ? CommonConstants.CONTAINS_COMPARATOR
							: CommonConstants.IN_COMPARATOR;
			String[] filters = filterObject.split(comparator);
			String filterName = filters[0];
			String filterValue = filters[1];
			if (filterValue.equals(CommonConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID)) {
				filterHolder.add(new Filter(filterName, comparator, LoggedInUserInstance.id));
			}
		}
		return filterHolder;
	}

	private boolean joinRequired(Path<?> path, String part) {
		try {
			return path.get(part).getJavaType().getName().startsWith("java.lang") ? false : true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public Map<String, Object> listUsers(Integer pageNumber, Integer pageSize, String sortField, String sortDirection,
			String filter, String fields) throws BaseException {
		List<Filter> filters = filter == null ? null : formatFilters(filter);
		List<Predicate> predicates = new ArrayList<>();
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);

		Root<LocalUser> r = query.from(LocalUser.class);

		List<String> pluralAttributes = r.getModel().getPluralAttributes().stream().map(obj -> obj.getName())
				.collect(Collectors.toList());
		if (fields != null && !fields.isEmpty()) {
			List<Selection<?>> s = new LinkedList<Selection<?>>();
			String[] fieldsRequested = fields.split(",");
			for (String field : fieldsRequested) {
				if (field.contains(".")) {
					Path<?> path = r;
					for (String part : field.split("\\.")) {
						if (pluralAttributes.contains(part) || joinRequired(path, part)) {
							path = r.join(part, JoinType.LEFT);
						} else {
							path = path.get(part);
						}
					}
					s.add(path);
				} else {
					s.add(r.get(field));
				}
			}
			query.multiselect(s);
		}

		List<Predicate> predicatesGenerated = FiltersPredicateUtil.generatePredicatesFilters(builder, r, filters);

		if (predicatesGenerated != null && !predicatesGenerated.isEmpty())
			predicates.addAll(predicatesGenerated);

		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

		if (sortField != null && sortDirection != null) {
			if (sortDirection.equals(CommonConstants.SORT_DIRECTION_ASC)) {
				query.orderBy(builder.asc(r.get(sortField)));
			} else if (sortDirection.equals(CommonConstants.SORT_DIRECTION_DESC)) {
				query.orderBy(builder.desc(r.get(sortField)));
			}
		} else {
			query.orderBy(builder.desc(r.get(CommonConstants.ID)));
		}

		List<Tuple> result = entityManager.createQuery(query)
				.setFirstResult((pageNumber - 1) * pageSize).setMaxResults(pageSize).getResultList();
		String[] fieldsArr = fields.split(",");
		List<Map<String, Object>> response = new ArrayList<>();
		for (Tuple obj : result) {
			Map<String, Object> responseObj = new HashMap<>();
			for (int i = 0; i < fieldsArr.length; i++) {
				String fieldName = fieldsArr[i];
				String key = fieldName.contains(".") ? fieldName.split("\\.")[0] : fieldName;
				responseObj.put(key, obj.get(i));
			}
			response.add(responseObj);
		}
		
		int totalCount = entityManager.createQuery(query).getResultList().size();

		Map<String, Object> dataToBeReturned = new HashMap<>();
		dataToBeReturned.put(CommonConstants.RESULTS, response);
		dataToBeReturned.put(CommonConstants.TOTAL_COUNT, totalCount);

		int lastPage = 0;
		if (totalCount % pageSize == 0) {
			lastPage = totalCount / pageSize;
		} else {
			lastPage = ((totalCount / pageSize) + 1);
		}
		dataToBeReturned.put(CommonConstants.LAST_PAGE, lastPage);

		return dataToBeReturned;
	}
}
