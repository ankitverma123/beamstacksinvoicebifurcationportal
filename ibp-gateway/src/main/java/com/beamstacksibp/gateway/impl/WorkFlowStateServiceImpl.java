package com.beamstacksibp.gateway.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.WorkFlowState;
import com.beamstacksibp.gateway.repository.IWorkFlowStateRepository;
import com.beamstacksibp.gateway.service.IWorkFlowStateService;

/**
 * @author AmkuGlory
 *
 */
@Service
public class WorkFlowStateServiceImpl implements IWorkFlowStateService {

	@Autowired
	private IWorkFlowStateRepository workFlowStateRepository;

	@Override
	public WorkFlowState getWorkFlowState(String identifier) {
		return workFlowStateRepository.findWorkFlowStateByIdentifier(identifier);
	}

}
