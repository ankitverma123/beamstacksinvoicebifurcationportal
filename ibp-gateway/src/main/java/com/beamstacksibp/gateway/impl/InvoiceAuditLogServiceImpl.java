package com.beamstacksibp.gateway.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.dto.LoggedInUserInstance;
import com.beamstacksibp.gateway.entity.InvoiceAuditLog;
import com.beamstacksibp.gateway.repository.IInvoiceAuditLogRepository;
import com.beamstacksibp.gateway.service.IInvoiceAuditLogService;

/**
 * @author AmkuGlory
 *
 */
@Service
public class InvoiceAuditLogServiceImpl implements IInvoiceAuditLogService {

	@Autowired
	private IInvoiceAuditLogRepository auditLogRepo;

	@Autowired
	private EntityManager entityManager;

	@Override
	public void saveAuditLog(UUID invoiceUUID, UUID workFlowState) {
		InvoiceAuditLog invoiceAuditLog = new InvoiceAuditLog();
		invoiceAuditLog.setInvoice(invoiceUUID);
		invoiceAuditLog.setWorkFlowState(workFlowState);
		invoiceAuditLog.setUserMapped(LoggedInUserInstance.id);
		auditLogRepo.save(invoiceAuditLog);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String rejectedBy(UUID invoiceUUID, UUID workFlowState) {

		String query = "Select new map(localUser.firstName as firstName, localUser.lastName as lastName) "
				+ "FROM InvoiceAuditLog ial JOIN LocalUser localUser ON ial.userMapped = localUser.id "
				+ "WHERE ial.invoice = :invoiceUUID and ial.workFlowState = :workFlowState";
		Map<String, String> result = (Map<String, String>) entityManager.createQuery(query)
				.setParameter("invoiceUUID", invoiceUUID).setParameter("workFlowState", workFlowState)
				.getSingleResult();
		if (!result.isEmpty()) {
			return String.format("%s %s", result.get("firstName"), result.get("lastName"));
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> invoiceAuditLog(UUID invoice) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT new map(");
		sb.append("wfs.name as workFlowState, ");
		sb.append("lu.firstName as firstName, ");
		sb.append("lu.lastName as lastName, ");
		sb.append("role.name as roleName, ");
		sb.append("ir.reasonMsg as rejectionNote, ");
		sb.append("rr.reason as rejectionReason, ");
		//sb.append("ia.approvalNotes as approvalNotes, ");
		sb.append("ial.createdAt as createdAt ");
		sb.append(") FROM InvoiceAuditLog ial ");
		sb.append("LEFT OUTER JOIN InvoiceRejections ir ON ial.invoice = ir.mappedInvoice ");
		sb.append("LEFT OUTER JOIN RejectionReasons rr ON ir.rejectionReason = rr.id ");
		//sb.append("LEFT OUTER JOIN InvoiceApproval ia ON ial.invoice = ia.mappedInvoice ");
		sb.append("JOIN WorkFlowState wfs ON ial.workFlowState = wfs.id ");
		sb.append("JOIN LocalUser lu ON ial.userMapped = lu.id ");
		sb.append("JOIN lu.roles role ");
		sb.append("WHERE ial.invoice = :invoice ");
		sb.append("ORDER BY ial.createdAt ASC");

		return entityManager.createQuery(sb.toString()).setParameter("invoice", invoice).getResultList();
	}

}
