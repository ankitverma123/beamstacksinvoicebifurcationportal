package com.beamstacksibp.gateway.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.dto.PrivilegeDTO;
import com.beamstacksibp.gateway.entity.Privilege;
import com.beamstacksibp.gateway.repository.IPrivilegeRepository;
import com.beamstacksibp.gateway.service.IPrivilegeService;

/**
 * @author ankitkverma
 *
 */
@Service
public class PrivilegeServiceImpl implements IPrivilegeService{

	@Autowired
	private IPrivilegeRepository privilegeRepository;
	
	@Override
	public List<Privilege> findPrivileges() {
		return privilegeRepository.findAll();
	}

	@Override
	public Privilege findPrivileges(Long id) {
		return privilegeRepository.findPrivilegeById(id);
	}

	@Override
	public String deletePrivileges(Long id) {
		int deleteCount = privilegeRepository.deletePrivilegeById(id);
		return deleteCount > 0 ? "Privilege Deleted Successfully" : "Failed to delete privilege"; 
	}


	@Override
	public Object saveUpdate(PrivilegeDTO dto) {
		Privilege privilege = null;
		if(dto.getId()!=null)
			privilege = privilegeRepository.findPrivilegeById(dto.getId());
		
		if(dto.getId()==null && dto.getName()!=null)
			privilege = privilegeRepository.findPrivilegeByName(dto.getName());
		
		privilege = privilege==null?privilege=new Privilege():privilege;
		
		privilege.setName(dto.getName());
		
		privilege = privilegeRepository.save(privilege);
		
		return privilege;
	}

}
