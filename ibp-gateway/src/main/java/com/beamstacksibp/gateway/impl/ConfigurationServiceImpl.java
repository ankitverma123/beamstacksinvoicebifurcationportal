package com.beamstacksibp.gateway.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.service.IConfigurationService;

/**
 * @author AmkuGlory
 *
 */
@Service
public class ConfigurationServiceImpl implements IConfigurationService{
	
	@Autowired
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getQueryResults(String query) {
		return entityManager.createQuery(query).getResultList();
	}
	
	@Override
	public List<Map<String, String>> getConfigurations(String type) {
		String query = "SELECT new map(c.attributeName as attributeName, c.attributeValue as attributeValue) from Configuration c where c.type='"+type+"'";
		return getQueryResults(query);
	}

	@Override
	public List<Map<String, String>> getExcelConfigurations(String type) {
		String query = "SELECT new map(c.attributeName as fieldName, c.attributeValue as headerName) from Configuration c where c.type='"+type+"'";
		return getQueryResults(query);
	}

}
