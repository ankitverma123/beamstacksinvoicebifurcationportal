package com.beamstacksibp.gateway.security.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.beamstacksibp.gateway.entity.LocalUser;
import com.beamstacksibp.gateway.entity.Role;
import com.beamstacksibp.gateway.security.JwtUser;
import com.beamstacksibp.gateway.service.ILocalUserService;

/**
 * @author ankitkverma
 *
 */
@Service
@Transactional
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private ILocalUserService localUserRepository;
	
    @Override
    public JwtUser loadUserByUsername(String login) throws UsernameNotFoundException {
    	LocalUser user = localUserRepository.authenticateUser(login);
    	if(user==null) {
    		return null;
    	}
    	
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for (Role authority : user.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority.getName());
            grantedAuthorities.add(grantedAuthority);
        }
        
        org.springframework.security.core.userdetails.User loggedInUser = new org.springframework.security.core.userdetails.User(login, "{noop}"+user.getPassword(),
                grantedAuthorities);
        return new JwtUser(user.getId(), user.getUserName(), user.getFirstName(), user.getLastName(), user.getEmail(), "{noop}"+user.getPassword(), loggedInUser.getAuthorities(), Boolean.TRUE, null);
    }
    
}
