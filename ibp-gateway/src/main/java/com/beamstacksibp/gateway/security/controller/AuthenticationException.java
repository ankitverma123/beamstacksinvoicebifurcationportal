package com.beamstacksibp.gateway.security.controller;

/**
 * @author ankitkverma
 *
 */
public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
