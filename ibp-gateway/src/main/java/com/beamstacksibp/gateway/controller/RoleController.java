package com.beamstacksibp.gateway.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.dto.RoleDTO;
import com.beamstacksibp.gateway.entity.Role;
import com.beamstacksibp.gateway.service.IRoleService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/roles")
public class RoleController {
	
	@Autowired
	private IRoleService roleService;
	
	@RequestMapping(value="/",method=RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Object saveUpdateRole(@RequestBody RoleDTO dto){
		return roleService.saveUpdate(dto);
	}
	
	@RequestMapping(value="/assignPrivileges",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Object assignRolePermission(@RequestParam("privileges")Long[] privileges, @RequestParam("roleId")Long roleId) throws BaseException{
		return roleService.assignPrivileges(roleId, privileges);
	}
	
	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Role> fetchRole(){
		return roleService.findRoles();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Role fetchRole(@PathVariable Long id){
		return roleService.findRoles(id);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public String deleteRole(@PathVariable Long id){
		return roleService.deleteRole(id);
	}
	
	@RequestMapping(value="/list",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Map<String, Object>> fetchRoles(){
		return roleService.fetchRolesList();
	}
}
