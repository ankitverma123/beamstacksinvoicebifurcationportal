package com.beamstacksibp.gateway.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.ResponseData;
import com.beamstacksibp.gateway.service.ILocalUserService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/localuser")
public class LocalUserController {

	@Autowired
	private ILocalUserService localUserService;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData fetchLocalUser(@RequestParam Integer pageNumber, @RequestParam Integer pageSize,
			@RequestParam(required = false) String sortDirection, @RequestParam(required = false) String sortField,
			@RequestParam(required = false) String filter, @RequestParam(required = false) String fields)
			throws BaseException {
		Map<String, Object> response = localUserService.listUsers(pageNumber, pageSize, sortField, sortDirection,
				filter, fields);
		return new ResponseData(response, null, HttpStatus.OK, null);
	}

}
