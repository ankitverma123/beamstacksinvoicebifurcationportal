package com.beamstacksibp.gateway.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beamstacksibp.gateway.dto.PrivilegeDTO;
import com.beamstacksibp.gateway.entity.Privilege;
import com.beamstacksibp.gateway.service.IPrivilegeService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/privilege")
public class PrivilegeController {
	
	@Autowired
	private IPrivilegeService privilegeService; 
	
	@RequestMapping(value="/",method=RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Object saveUpdatePrivilege(@RequestBody PrivilegeDTO dto){
		return privilegeService.saveUpdate(dto);
	}
	
	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Privilege> fetchPrivilege(){
		return privilegeService.findPrivileges();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Privilege fetchPrivilege(@PathVariable Long id){
		return privilegeService.findPrivileges(id);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public String deletePrivilege(@PathVariable Long id){
		return privilegeService.deletePrivileges(id);
	}
}
