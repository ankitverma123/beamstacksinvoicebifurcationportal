package com.beamstacksibp.gateway.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beamstacksibp.gateway.common.ResponseData;
import com.beamstacksibp.gateway.service.INavigationService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/navbar")
public class NavigationController {
	
	@Autowired
	private INavigationService navigationService;
	
	@RequestMapping(value="/",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseData getLeftNavBar(HttpServletRequest request) {
		return new ResponseData(navigationService.getNavBar(request), "Success", HttpStatus.OK, null);
	}
	
}
