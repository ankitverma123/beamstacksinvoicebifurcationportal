package com.beamstacksibp.gateway.controller;

import java.io.File;
import java.util.HashMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/files")
public class FilesResource {

	@GetMapping("/")
	public HashMap<String,String> requestFile(@RequestParam("fileName") String fileName) {
		HashMap<String, String> response = new HashMap<>();
		File f= new File(fileName);
		if(f.exists()) {
			response.put("filePath", f.getAbsolutePath());
		}
		return response;
	}
	
}
