package com.beamstacksibp.gateway.controller;

import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.ResponseData;
import com.beamstacksibp.gateway.common.Utils;
import com.beamstacksibp.gateway.dto.LoggedInUserInstance;
import com.beamstacksibp.gateway.service.ISQLService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/sql")
public class SqlController {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private ISQLService sqlService;

	@GetMapping(value = "/bank/dropdown", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData bankDropDown() throws BaseException {
		String query = "SELECT new map(bank.id as id, bank.name as label) FROM Bank bank";
		return new ResponseData(entityManager.createQuery(query).getResultList(), null, HttpStatus.OK, null);
	}

	@GetMapping(value = "/bank/ica/dropdown/{bank_uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData bankIcaDropdown(@PathVariable String bank_uuid) throws BaseException {
		if (Utils.checkString(bank_uuid) && Utils.checkIfUUID(bank_uuid)) {
			String query = "SELECT new map(ica.id as id, ica.identifier as label) FROM ICA ica WHERE ica.bank.id='"
					+ UUID.fromString(bank_uuid) + "'";
			return new ResponseData(entityManager.createQuery(query).getResultList(), null, HttpStatus.OK, null);
		}
		return new ResponseData(null, "Missing required or invalid url parameter bank id.", HttpStatus.BAD_REQUEST,
				null);
	}

	@GetMapping(value = "/bank/ica/dropdown/my", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData bankIcaDropdown() throws BaseException {
		String query = "SELECT new map(localUserAllocatedIca.ica.id as id, localUserAllocatedIca.ica.identifier as label) FROM AllocatedICA localUserAllocatedIca WHERE localUserAllocatedIca.userAllocated.id = :loggedInUserId";
		return new ResponseData(entityManager.createQuery(query).setParameter("loggedInUserId", LoggedInUserInstance.id)
				.getResultList(), null, HttpStatus.OK, null);
	}

	@GetMapping(value = "/rejectReasons", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData rejectReasonsDropdown() throws BaseException {
		String query = "SELECT new map(rr.id as id, rr.reason as label) FROM RejectionReasons rr";
		return new ResponseData(entityManager.createQuery(query).getResultList(), null, HttpStatus.OK, null);
	}

	@GetMapping(value = "/workflowstates", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData workFlowStates() throws BaseException {
		String query = "SELECT new map(workFlowState.id as id, workFlowState.name as label) FROM WorkFlowState workFlowState";
		return new ResponseData(entityManager.createQuery(query).getResultList(), null, HttpStatus.OK, null);
	}

	@PostMapping(value = "/updateWorkFlowState", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public Object approveInvoice(@RequestBody Map<String, String> requestMap) throws BaseException {
		String query = "UPDATE WorkFlowState workFlowState SET workFlowState.name = :workFlowStateName WHERE workFlowState.id = :id";
		return new ResponseData(
				entityManager.createQuery(query).setParameter("workFlowStateName", requestMap.get("label"))
						.setParameter("id", UUID.fromString(requestMap.get("id"))).executeUpdate(),
				"Success", HttpStatus.OK, null);
	}

	@PostMapping(value = "/bankManager", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public Object createUpdateBank(@RequestBody Map<String, String> requestMap) throws BaseException {
		return new ResponseData(sqlService.insertUpdateBank(requestMap), "Success", HttpStatus.OK, null);
	}

	@PostMapping(value = "/icaManager", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object createUpdateIca(@RequestBody Map<String, String> requestMap) throws BaseException {
		return new ResponseData(sqlService.insertUpdateIca(requestMap), "Success", HttpStatus.OK, null);
	}

}
