package com.beamstacksibp.gateway.controller;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.beamstacksibp.gateway.common.BaseException;
import com.beamstacksibp.gateway.common.ResponseData;
import com.beamstacksibp.gateway.service.IInvoiceService;

/**
 * @author ankitkverma
 *
 */
@RestController
@RequestMapping("/rest/invoices")
public class InvoiceController {

	@Autowired
	private IInvoiceService invoiceService;

	@PostMapping(value = "/import", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object importInvoiceExcel(HttpServletRequest request, @RequestParam("file") MultipartFile file)
			throws BaseException {
		return new ResponseData(invoiceService.importInvoices(file, request), "Success", HttpStatus.OK, null);
	}

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData listCandidate(@RequestParam Integer pageNumber, @RequestParam Integer pageSize,
			@RequestParam(required = false) String sortDirection, @RequestParam(required = false) String sortField,
			@RequestParam(required = false) String filter)
			throws BaseException {
		return new ResponseData(
				invoiceService.listInvoices(pageNumber, pageSize, sortField, sortDirection, filter), null,
				HttpStatus.OK, null);
	}

	@PostMapping(value = "/approve", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object approveInvoice(@RequestBody Map<String, String> requestMap) throws BaseException {
		return new ResponseData(invoiceService.approveRejectInvoice(requestMap), "Success", HttpStatus.OK, null);
	}

	@PostMapping(value = "/reject", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object rejectInvoice(@RequestBody Map<String, String> requestMap) throws BaseException {
		return new ResponseData(invoiceService.approveRejectInvoice(requestMap), "Success", HttpStatus.OK, null);
	}
	
	@GetMapping(value = "/auditLog/{invoiceUUID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object invoiceAuditLog(@PathVariable String invoiceUUID) throws BaseException {
		return new ResponseData(invoiceService.invoiceAuditLog(UUID.fromString(invoiceUUID)), "Success", HttpStatus.OK, null);
	}
}
