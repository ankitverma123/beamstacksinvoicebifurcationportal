package com.beamstacksibp.gateway.common;

import java.util.Map;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ankitkverma
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData {
	private Object data;
	private String message;
	private HttpStatus status;
	private Map<String, Object> metadata;
}
