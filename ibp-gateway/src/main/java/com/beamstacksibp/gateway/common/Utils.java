package com.beamstacksibp.gateway.common;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author AmkuGlory
 *
 */
public class Utils {
	/**
	 * @param map (Excepts object of HashMap of type Map<String, Object> map)
	 * @param key (Type of string)
	 * @return True if the key exist in the collection and is not null or empty.
	 */
	public static boolean collectionKeyFieldChecker(Map<String, Object> map, String key) {
		return map.containsKey(key) && map.get(key) != null && !map.get(key).toString().trim().isEmpty();
	}

	public static boolean checkString(String str) {
		return str != null && !str.isEmpty();
	}

	/**
	 * @param uuidString
	 * @return True if the supplied string is of UUID format or not
	 */
	public static boolean checkIfUUID(String uuidString) {
		if (!checkString(uuidString))
			return true;
		Pattern p = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$");
		return p.matcher(uuidString).matches();
	}

	/**
	 * @param row This expects an object of excel row
	 * @return True if row is empty otherwise False
	 */
	public static boolean isRowEmpty(Row row) {
		if (row == null) {
			return true;
		}
		if (row.getLastCellNum() <= 0) {
			return true;
		}
		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != org.apache.poi.ss.usermodel.CellType.BLANK)
				return false;
		}
		return true;
	}

	/**
	 * @param obj
	 * Temporary method, which is being used for debugging purposes to write collections as a JSON string 
	 */
	public static void printObjectAsString(Object obj) {
		try {
			System.out.println("\n**************************************************");
			System.out.println(new ObjectMapper().writeValueAsString(obj));
			System.out.println("**************************************************\n");
		} catch (JsonProcessingException e) {
			System.out.println("\n**************************************************");
			System.out.println("JsonProcessingException= " + e.getMessage());
			System.out.println("**************************************************\n");
		}
	}

}
