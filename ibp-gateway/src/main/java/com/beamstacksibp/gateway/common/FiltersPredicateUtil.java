package com.beamstacksibp.gateway.common;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author ankitkverma
 *
 */
public class FiltersPredicateUtil {

	private FiltersPredicateUtil() {
		throw new IllegalStateException("Utility class");
	}

	static List<Predicate> predicates = new ArrayList<Predicate>();

	public static List<Predicate> generatePredicatesFilters(CriteriaBuilder builder, Root<?> r, List<Filter> filters) {
		predicates.clear();
		if (filters != null && !filters.isEmpty()) {
			System.out.println(filters);
			for (Filter filter : filters) {
				Object value = Utils.checkIfUUID(filter.getValue().toString()) ? UUID.fromString(filter.getValue().toString()) : filter.getValue().toString();
				switch (filter.getComparison()) {
				case CommonConstants.EQUAL_COMPARATOR:
					predicates.add(builder.equal(getPath(r, filter.getColumn()), value));
					break;
				case CommonConstants.IN_COMPARATOR:
					Path<?> p = getPath(r, filter.getColumn());
					Predicate parentPredicate = p.in(filter.getValue());
					predicates.add(parentPredicate);
					break;
				}
			}
		}
		return predicates;
	}

	private static Path<?> getPath(Root<?> r, String attributeName) {
		Path<?> path = r;
		for (String part : attributeName.split("\\.")) {
			path = path.get(part);
		}
		return path;
	}

}
