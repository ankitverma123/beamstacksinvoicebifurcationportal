package com.beamstacksibp.gateway.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Data
@AllArgsConstructor
public class Filter {
	private String column;
	private String comparison;
	private Object value;
}
