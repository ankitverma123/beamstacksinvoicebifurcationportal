package com.beamstacksibp.gateway.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author ankitkverma
 *
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        String exception = ex.getMessage();
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if(exception.startsWith("Authentication") || exception.startsWith("Authorization")){
        	status = HttpStatus.FORBIDDEN;
        }
        ResponseData error = new ResponseData(null, ex.getMessage(), status, null);
        return new ResponseEntity<Object>(error, status);
    }
	
}
