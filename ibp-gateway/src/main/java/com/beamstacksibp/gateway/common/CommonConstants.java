package com.beamstacksibp.gateway.common;

/**
 * @author ankitkverma
 *
 */
public class CommonConstants {
	public static final String SUCCESS = " SUCCESS";
	public static final String FAILURE = " FAILURE";
	
	public static final String WORK_FLOW_STATE_SENT_BY_AGENT = "1001";
	public static final String WORK_FLOW_STATE_SENT_BY_ISSUER = "1002";
	public static final String WORK_FLOW_STATE_SENT_BY_ACQUIRER = "1003";
	public static final String WORK_FLOW_STATE_REJECTED_BY_ISSUER = "1004";
	public static final String WORK_FLOW_STATE_REJECTED_BY_ACQUIRER = "1005";
	public static final String WORK_FLOW_STATE_APPROVED_BY_ISSUER = "1006";
	public static final String WORK_FLOW_STATE_APPROVED_BY_ACQUIRER = "1007";
	
	public static final String SPECIAL_FILTER_NAME_LOGGEDIN_USERID = "loggedInUserId";
	public static final String SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK = "myAllocatedBank";
	public static final String SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA = "myAllocatedBankIcaList";
	
	public static final String EQUAL_COMPARATOR = "_eq_";
	public static final String CONTAINS_COMPARATOR = "_con_";
	public static final String IN_COMPARATOR = "_in_";
	
	public static final String SORT_DIRECTION_ASC = "asc";
	public static final String SORT_DIRECTION_DESC = "desc";
	public static final String RESULTS = "results";
	public static final String TOTAL_COUNT = "totalCount";
	public static final String LAST_PAGE = "lastPage";

	public static final String INVOICE_EXCEL_TEMPLATE = "InvoiceTemplate";
	public static final String ISSUER_ID = "issuerId";
	public static final String ACQUIRER_ID = "acquirerId";
	public static final String FILE_TRACKING_ID = "fileTrackingId";
	public static final String ID = "id";

	public static final String PRIVILEGE_CREATED_SUCCESS = "New Privilege Created Successfully";
	public static final String PRIVILEGE_MODIFIED_SUCCESS = "Privilege Modified Successfully";
	public static final String PRIVILEGE_DELETED_SUCCESS = "Privilege Deleted Successfully";

	public static final String PRIVILEGE_CREATED_FAILURE = "Failed to create new privilege";
	public static final String PRIVILEGE_MODIFIED_FAILURE = "Failed to modify privilege";
	public static final String PRIVILEGE_DELETED_FAILURE = "Failed to delete privilege";

	public static final String PRIVILEGE_COUNT = " Privileges Found";
	public static final String INVALID_PRIVILEGE_ID = " Invalid Privilege ID Found";

	public static final String ROLE_COUNT = " Roles Found";
	public static final String INVALID_ROLE_ID = " Invalid Role ID Found";
	public static final String ROLE_DELETED_SUCCESS = "Role Deleted Successfully";

	public static String createdSuccessfully(String entity) {
		return "New " + entity + " Created Successfully";
	}

	public static String addedSuccessfully(String entity) {
		return "New " + entity + " Created Successfully";
	}

	public static String addedFailure(String entity) {
		return "Failed to add " + entity;
	}

	public static String modifiedSuccessfully(String entity) {
		return entity + " Modified Successfully";
	}

	public static String DeletedSuccessfully(String entity) {
		return entity + " Deleted Successfully";
	}

	public static String createdFailure(String entity) {
		return "Failed to create new " + entity;
	}

	public static String modifiedFailure(String entity) {
		return "Failed to modify " + entity;
	}

	public static String DeletedFailure(String entity) {
		return "Failed to delete " + entity;
	}

}
