package com.beamstacksibp.gateway.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.beamstacksibp.gateway.security.JwtAuthenticationEntryPoint;
import com.beamstacksibp.gateway.security.JwtAuthorizationTokenFilter;
import com.beamstacksibp.gateway.security.service.JwtUserDetailsService;

/**
 * @author ankitkverma
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    // Custom JWT based security filter
    @Autowired
    JwtAuthorizationTokenFilter authenticationTokenFilter;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.route.authentication.path}")
    private String authenticationPath;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService);
    }

   /* @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }*/

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
    	
    	List<String> csrfIncludeRegexPatterns = new ArrayList<String>();
    	List<String> csrfExcludeRegexPatterns = new ArrayList<String>();
    	csrfIncludeRegexPatterns.add("/rest/.*");
    	//csrfExcludeRegexPatterns.add("/fillSurvey");
    	
        httpSecurity
            // we don't need CSRF because our token is invulnerable
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/login",
                    "/home",
                    "/configuration",
                    "/localuser",
                    "/newuser",
                    "/launchSurvey",
                    "/surveyconf",
                    "/discoverydashboard",
                    "/fulldashboard",
                    "/emailTemplates",
                    "/reports",
                    "/groupexclusion",
                    "/templateEditor/**",
                    "/configuration/**",
                    "/newuser/**","/auth/**","/*.js", "/files/**").permitAll()
            .requestMatchers(new CustomRequestMatcher(csrfIncludeRegexPatterns, csrfExcludeRegexPatterns)).authenticated()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

       httpSecurity
            .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // AuthenticationTokenFilter will ignore the below paths
        web
            .ignoring()
            .antMatchers(
                HttpMethod.POST,
                authenticationPath
            )

            // allow anonymous resource requests
            .and()
            .ignoring()
            .antMatchers(
                HttpMethod.GET,
                "/",
                "/login",
                "/home",
                "/configuration",
                "/configuration/**",
                "/localuser",
                "/newuser",
                "/launchSurvey",
                "/surveyconf",
                "/discoverydashboard",
                "/fulldashboard",
                "/emailTemplates",
                "/reports",
                "/groupexclusion",
                "/templateEditor/**",
                "/newuser/**",
                "/*.html",
                "/rest/survey/**",
                "/rest/report/**",
                "/favicon.ico",
                "/assets/*",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js",
                "/**/*.png",
                "/**/*.jpg",
                "/**/*.jpeg",
                "/socket/**",
                "/files"
            )
            .and()
            .ignoring()
            .antMatchers(
            	HttpMethod.PUT,
            	"/rest/survey/**"
            )
            .and().ignoring().antMatchers(HttpMethod.GET, "/files/**");
    }
}
