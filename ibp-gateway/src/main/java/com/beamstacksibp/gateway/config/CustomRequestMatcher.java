package com.beamstacksibp.gateway.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class CustomRequestMatcher implements RequestMatcher{

	List<RequestMatcher> includeResourcePatterns = null;
	List<RequestMatcher> excludingResourcePatterns = null;

	@Override
	public boolean matches(HttpServletRequest request) {
		for (RequestMatcher matcher : excludingResourcePatterns) {
			if (matcher.matches(request)) {
				return false;
			}
		}
		for (RequestMatcher matcher : includeResourcePatterns) {
			if (matcher.matches(request)) {
				return true;
			}
		}

		return false;
	}

	public CustomRequestMatcher(List<String> includeResourcePatterns, List<String> excludingResourcePatterns) {
		this.includeResourcePatterns = loadRegexPattern(includeResourcePatterns);
		this.excludingResourcePatterns = loadRegexPattern(excludingResourcePatterns);
	}

	/**
	 * load the regex pattern at server startup
	 * 
	 * @param regexPatterns
	 * @return
	 */
	private List<RequestMatcher> loadRegexPattern(List<String> regexPatterns) {
		List<RequestMatcher> matchers = new ArrayList<RequestMatcher>();
		for (String pattern : regexPatterns) {
			matchers.add(new RegexRequestMatcher(pattern, null));
		}
		return matchers;
	}

}
