export class AppConstants {
  public static BANK_SENT_INVOICES_AGENT_DASHBOARD = 'Bank::Sent::Invoices::Agent';
  public static BANK_RECEIVED_INVOICES_AGENT_DASHBOARD = 'Bank::Received::Invoices::Agent';

  public static AGENT_SENT_INVOICES_GRID_REST_END_POINT = '/rest/invoices/';
  public static LOCAL_USERS_GRID_REST_END_POINT = '/rest/localuser/?fields=id,firstName,lastName,userName,email,bank.name,roles.name';
  public static INVOICES_GRID_REST_END_POINT = '/rest/invoices/';
  public static BANK_DROPDOWN_REST_END_POINT = '/rest/sql/bank/dropdown';
  public static BANK_ICA_REST_END_POINT = '/rest/sql/bank/ica/dropdown/';
  public static MY_ASSIGNED_ICA_REST_END_POINT = '/rest/sql/bank/ica/dropdown/my';
  public static APPROVE_INVOICE_REST_END_POINT = '/rest/invoices/approve';
  public static REJECT_INVOICE_REST_END_POINT = '/rest/invoices/reject';
  public static REJECT_REASONS_REST_END_POINT = '/rest/sql/rejectReasons';
  public static WORK_FLOW_STATES_REST_END_POINT = '/rest/sql/workflowstates';
  public static UPDATE_WORK_FLOW_STATES_REST_END_POINT = '/rest/sql/updateWorkFlowState';
  public static BANK_CREAT_UPDATE_REST_END_POINT = '/rest/sql/bankManager';
  public static ICA_CREAT_UPDATE_REST_END_POINT = '/rest/sql/icaManager';
  public static INVOICES_AUDIT_LOG_REST_END_POINT = '/rest/invoices/auditLog/';

  public static ROLE_AGENT = 'Agent';
  public static ROLE_ADMIN = 'Admin';
  public static ROLE_ISSUER = 'Issuer';
  public static ROLE_ACQUIRER = 'Acquirer';

  public static SPECIAL_FILTER_NAME_LOGGEDIN_USERID = 'loggedInUserId';
  public static SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK = 'myAllocatedBank';
  public static SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA = 'myAllocatedBankIcaList';
  
  public static WORK_FLOW_STATE_SENT_BY_AGENT = "1001";
	public static WORK_FLOW_STATE_SENT_BY_ISSUER = "1002";
	public static WORK_FLOW_STATE_SENT_BY_ACQUIRER = "1003";
	public static WORK_FLOW_STATE_REJECTED_BY_ISSUER = "1004";
	public static WORK_FLOW_STATE_REJECTED_BY_ACQUIRER = "1005";
	public static WORK_FLOW_STATE_APPROVED_BY_ISSUER = "1006";
  public static WORK_FLOW_STATE_APPROVED_BY_ACQUIRER = "1007";
  
}
