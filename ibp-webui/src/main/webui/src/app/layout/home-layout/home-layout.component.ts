import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavbarService } from '../../_services/Navigation/navbar.service';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css'],
  providers: [NavbarService]
})
export class HomeLayoutComponent implements OnInit {
  opened: boolean;
  folders = [];
  constructor(private router: Router,private sidenavServie: NavbarService, private route: ActivatedRoute) {
    this.sidenavServie.getNavBar().subscribe((result)=>{
      this.folders = result.data;
      if (this.router.url === '/') {
        this.router.navigate([this.folders[0].path]);
      } else {
        const urlPattern = this.router.url;
        if (urlPattern.indexOf('?') > -1) {
          const urlPatternSplit = urlPattern.split('?');
          const url = urlPatternSplit[0];
          const queryParams = urlPattern[1].split('&');
          let queryParameterCollection = {};
          queryParams.forEach( param => {
            const params = param.split('=');
            queryParameterCollection[params[0]] = queryParameterCollection[params[1]];
          });
          this.router.navigate([url], { queryParams: queryParameterCollection });
        } else {
        this.router.navigate([this.router.url]);
        }
      }
    });

}

navBarClkHandler(path){
  this.router.navigate([path]);
  this.opened = !this.opened;
}

  talkBack(e: string) {
    this.opened = !this.opened;
  }

  ngOnInit() {
  }

}
