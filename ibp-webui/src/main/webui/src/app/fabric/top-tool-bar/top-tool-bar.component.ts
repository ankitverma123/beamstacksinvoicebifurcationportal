import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services/auth/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { TokenStorage } from '../../guards/TokenStorage';
import { TabNavigationService } from '../../_services/common/tabNavigation.service';
import { CommonService } from 'src/app/_services/common/common.service';

@Component({
  selector: 'app-top-tool-bar',
  templateUrl: './top-tool-bar.component.html',
  styleUrls: ['./top-tool-bar.component.css']
})
export class TopToolBarComponent implements OnInit {
  @Input() navigationDataSet = [];
  loggedInUser: string;
  constructor(private router :Router,private authService : AuthenticationService,
     private cookieService: CookieService, private tokenStorage: TokenStorage, 
     private commonService: CommonService)  {
      this.loggedInUser = `${commonService.loggedInUserFirstName()} ${commonService.loggedInUserLastName()}`;
    }

  ngOnInit() {
    console.log(this.navigationDataSet)
  }

  @Output() talk: EventEmitter<string> = new EventEmitter<string>();
  talkBack(say: string) {
    this.talk.emit(say);
  }

  logout() {
    this.tokenStorage.signOut();
    this.cookieService.deleteAll();
    this.authService.logout();
  }

}
