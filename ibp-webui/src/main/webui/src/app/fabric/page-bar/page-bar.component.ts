import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page-bar',
  templateUrl: './page-bar.component.html',
  styleUrls: ['./page-bar.component.css']
})
export class PageBarComponent implements OnInit {

  @Input() pageTitle : String;
  @Input() buttons?: any;

  constructor() { }

  ngOnInit() {
  }

}
