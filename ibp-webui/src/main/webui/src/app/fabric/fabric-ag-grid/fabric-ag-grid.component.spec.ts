import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricAgGridComponent } from './fabric-ag-grid.component';

describe('FabricAgGridComponent', () => {
  let component: FabricAgGridComponent;
  let fixture: ComponentFixture<FabricAgGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricAgGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricAgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
