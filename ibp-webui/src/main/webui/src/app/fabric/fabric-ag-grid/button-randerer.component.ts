import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'app-button-renderer',
    template: `
    <button style="margin-right: 3px;"
    class="{{btn.class}}"
    type="button"
    (click)="btn.clkHandler(this.params.data)"
    *ngFor='let btn of label'>
    {{btn.label}}
    </button>
    `
})

export class ButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: any;

    agInit(params): void {
        this.params = params;
        this.label = this.params.label || null;
    }

    refresh(params?: any): boolean {
        return true;
    }

    onClick($event) {
        if (this.params.onClick instanceof Function) {
            // put anything into params u want pass into parents component
            const params = {
                event: $event,
                rowData: this.params.node.data
                // ...something
            };
            this.params.onClick(params);

        }
    }
}
