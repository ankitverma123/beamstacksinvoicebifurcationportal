import { Component, OnInit, Input } from '@angular/core';
import { Subject, Subscription } from '../../../../node_modules/rxjs';
import { GridOptions } from 'ag-grid-community';
import { ButtonRendererComponent } from './button-randerer.component';
import { IconbuttonRendererComponent } from './iconbutton-renderer.component';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

@Component({
  selector: 'app-fabric-ag-grid',
  templateUrl: './fabric-ag-grid.component.html',
  styleUrls: ['./fabric-ag-grid.component.css'],
  entryComponents: [ButtonRendererComponent, IconbuttonRendererComponent]
})
export class FabricAgGridComponent implements OnInit {
  
  @Input() gridToBeRefresh: Subject<any>;
  @Input() restUrl: any;
  @Input() columnDefs: any;
  @Input() frameworkComponents: any;
  @Input() staticData: any;
  
  private gridOptions: GridOptions;
  private gridApi;
  private gridColumnApi;

  filterCriteria = null;
  gridToBeRefreshSubscription: Subscription;

  // Pagination Button Controls
  firstPageButtonDisabled = false;
  previousPageButtonDisabled = false;
  nextPageButtonDisabled = false;
  lastPageButtonDisabled = false;


  // Pagination controls
  paginationPageSize = 5;
  totalCount = 0;
  pageNumber = 1;
  totalPages = 0;
  lastPage = 1;

  // Grid Controls
  rowHeight = 35;
  

  defaultColDef = { filter: true };

  rowData = [];

  constructor(private httpClient: HttpClient) {
   }

  ngOnInit() {
    this.gridToBeRefreshSubscription = this.gridToBeRefresh.subscribe((event: any) => {
      if (event.hasOwnProperty('updateGridFilter')) {
        this.pageNumber = 1;
        this.filterCriteria = event.updateGridFilter.join(',');
      }
      if (event.hasOwnProperty('reload')) {
        this.pageNumber = 1;
      }
      this.populateRowData();
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.sizeColumnsToFit();

    params.api.sizeColumnsToFit();
    params.api.setHeaderHeight(50);
    window.addEventListener('resize', () => {
      setTimeout(() => {
        params.api.sizeColumnsToFit();
      });
    });
    this.populateRowData();
  }

  onPageSizeChanged() {
    this.gridApi.paginationSetPageSize(Number(this.paginationPageSize));
    this.populateRowData();
  }

  populateRowData() {
    if(this.staticData && !this.restUrl){
      this.rowData = this.staticData;
      this.totalCount = this.staticData.length;
      this.totalPages = Math.ceil(this.totalCount/this.paginationPageSize);
      this.paginationPageButtonStateEvaluator();
    } else {
      let restEndPoint = `${this.restUrl}`;
      if(this.restUrl.includes('filter')){
        if (this.filterCriteria !== null) {
          restEndPoint += ',' + this.filterCriteria;
        }
        restEndPoint += `&pageNumber=${this.pageNumber}&pageSize=${this.paginationPageSize}`;
      } else {
        if(this.restUrl.includes('?')){
          restEndPoint += '&';
        } else {
          restEndPoint += '?';
        }
        restEndPoint += `pageNumber=${this.pageNumber}&pageSize=${this.paginationPageSize}`;
        if (this.filterCriteria !== null) {
          restEndPoint += '&filter=' + this.filterCriteria;
        }
      }
      
      this.gridApi.showLoadingOverlay();
      this.httpClient.get(restEndPoint).subscribe((res: any) => {
        this.rowData = res.data.results;
        this.totalCount = res.data.totalCount;
        this.lastPage = res.data.lastPage;
        this.totalPages = res.data.lastPage;
        this.paginationPageButtonStateEvaluator();
      }, error => {
        this.rowData = [];
        this.totalCount = 0;
      });
    }
  }

  goToFirstPage() {
    this.pageNumber = 1;
    this.paginationPageButtonStateEvaluator();
    this.populateRowData();
  }

  goToLastPage() {
    this.pageNumber = this.lastPage;
    this.paginationPageButtonStateEvaluator();
    this.populateRowData();
  }

  goToNextPage() {
    this.pageNumber += 1;
    this.paginationPageButtonStateEvaluator();
    this.populateRowData();
  }

  goToPreviousPage() {
    this.pageNumber -= 1;
    this.paginationPageButtonStateEvaluator();
    this.populateRowData();
  }

  paginationPageButtonStateEvaluator(){
    if (this.pageNumber == 1) {
      this.firstPageButtonDisabled = true;
      this.previousPageButtonDisabled = true;
    } else if (this.pageNumber > 1) {
      this.firstPageButtonDisabled = false;
      this.previousPageButtonDisabled = false;
    }
    this.nextPageButtonDisabled = this.pageNumber == this.lastPage || ((Number(this.pageNumber) * Number(this.paginationPageSize)) > this.totalCount);
    this.lastPageButtonDisabled = this.pageNumber == this.lastPage || this.lastPage === 1 ;
  }

  ngOnDestroy() {
    this.gridToBeRefreshSubscription.unsubscribe();
  }

  
}
