import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricGridComponent } from './fabric-grid.component';

describe('FabricGridComponent', () => {
  let component: FabricGridComponent;
  let fixture: ComponentFixture<FabricGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
