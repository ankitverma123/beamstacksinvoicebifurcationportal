import { CustomColumnButttons } from './custom-column-buttons';
import { CustomColumnIcons } from './custom-column-icons';
import { Component } from '@angular/core/src/metadata/directives';

export class CustomColumns {
    title: string;
    type: string;
    sequence: number;
    cssClass: String;
    includeComponent: Component;
    showGridFilter: boolean = true;
    buttonsOrIcons: CustomColumnButttons[] | CustomColumnIcons[];

    constructor(options: {
        title?: string,
        type?: string,
        sequence?: number,
        cssClass?: String,
        includeComponent?: any,
        showGridFilter?: boolean,
        buttonsOrIcons?: CustomColumnButttons[] | CustomColumnIcons[],
    } = {}) {
        this.title = options.title;
        //this.type = options.type;
        this.sequence = options.sequence;
        this.cssClass = options.cssClass;
        this.includeComponent = options.includeComponent;
        this.showGridFilter = options.showGridFilter;
        this.buttonsOrIcons = options.buttonsOrIcons;
        if (options && options.buttonsOrIcons && options.buttonsOrIcons.length > 0 && (options.buttonsOrIcons[0] instanceof CustomColumnIcons)) {
            this.type = 'icon';
        } else {
            this.type = 'button';
        }
    }
}