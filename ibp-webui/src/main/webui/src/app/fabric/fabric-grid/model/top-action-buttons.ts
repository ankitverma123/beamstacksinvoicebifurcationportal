export class TopActionButtons {
    iconName: string;
    tooltip: string;
    isDisabled: boolean;
    btnClkHandler: Function;

    constructor(private iconNme: string, private tooltp: string, private isDisabld: boolean, private btnHandler: Function) {
        this.iconName = iconNme;
        this.tooltip = tooltp;
        this.isDisabled = isDisabld;
        this.btnClkHandler = btnHandler;
    }
}