export class CustomColumnButttons {
    label: string;
    isDisabled: Boolean;
    mappingField: string;
    customColumnClkHandler: Function;

    constructor(label: string, isDisabled: Boolean, mappingField: string, customColumnClkHandler: Function) {
        this.label = label;
        this.isDisabled = isDisabled;
        this.mappingField = mappingField;
        this.customColumnClkHandler = customColumnClkHandler;
    }
}