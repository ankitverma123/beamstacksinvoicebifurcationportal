import {
    Component, NgModule, Compiler, Directive, Output, Input, EventEmitter, ViewContainerRef, Inject, OnInit, ComponentFactoryResolver, ViewChild
} from "@angular/core";
import { CommonModule } from '@angular/common';
import { ComponentFactoryBoundToModule } from "@angular/core/src/linker/component_factory_resolver";
import { viewClassName } from "@angular/compiler";

@NgModule({
    imports: [CommonModule],
})
class DynamicHtmlModule { }

@Directive({
    selector: 'fabric-grid-comp-include'
})
export class FabricGridCompInclude {
    @Input() component: Component;
    @Input() data;
    @Input() column;
    // @Input() rowIndex;
    @Output() dataFromComponent = new EventEmitter();
    factoryResolver;
    rootViewContainer;
    viewRef;
    constructor(@Inject(ComponentFactoryResolver) factoryResolver, @Inject(ViewContainerRef) viewContainerRef) {
        this.factoryResolver = factoryResolver;
        this.viewRef = viewContainerRef;
    }
    setRootViewContainerRef(viewContainerRef) {
        this.rootViewContainer = viewContainerRef
    }
    ngOnChanges() {
        if (!this.component) return;
        this.setRootViewContainerRef(this.viewRef);
        const factory = this.factoryResolver.resolveComponentFactory(this.component);
        const component = factory.create(this.rootViewContainer.parentInjector);
        if (this.data) {
            component.instance.data = this.data;
        }
        // if(this.rowIndex){
        //   component.instance.rowIndex = this.rowIndex;
        // }
        if (component.instance.observeVariable) {
            component.instance.observeVariable.subscribe(result => {
                this.dataFromComponent.emit(result);
            })
        }
        const componentRef = this.rootViewContainer.insert(component.hostView);
    }
}