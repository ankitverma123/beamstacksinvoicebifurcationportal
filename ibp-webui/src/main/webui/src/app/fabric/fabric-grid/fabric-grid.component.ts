import { Component, OnInit, ViewChild, Input, Output, EventEmitter,SimpleChanges,OnChanges, Inject, ChangeDetectorRef,NgZone} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator,MatIconRegistry,MatSort } from '@angular/material';
import { Restangular } from 'ngx-restangular';
import { SelectionModel} from '@angular/cdk/collections';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FabircGridDataSource } from './model/fabric-grid-data-source';
import { GridColumnHeaders } from './model/grid-column-headers';
import { CustomColumns } from './model/custom-columns';
import { TopActionButtons } from './model/top-action-buttons';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fabric-grid',
  templateUrl: './fabric-grid.component.html',
  styleUrls: ['./fabric-grid.component.css']
})
export class FabricGridComponent implements OnInit {
  @Input() restUrl;
  @Input() staticData = [];
  @Input() gridColumnHeaders: GridColumnHeaders[];
  @Input() gridColumnUrl: string;
  @Input() topActionButtons: TopActionButtons[];
  @Input() gridHeaderText: string;
  @Input() customColumns: CustomColumns[];
  @Input() dynamicCustomColumns: CustomColumns[]; // replacing gridHeader column with dynamic component to make custom column
  @Input() resultProperty: string;
  @Input() resultTotalCountProperty: string;
  @Input() firstResultProperty: string;
  @Input() fetchSizeProperty: string;
  @Input() paginationLimitOptions;
  @Input() pageLimit: number;
  @Input() initialPage: number;
  @Input() enableRowSelection: boolean;
  @Input() singleSelect: boolean;
  @Input() selectedItems = [];
  @Input() restUrlChange: boolean;
  @Input() updateGridData: boolean;
  @Input() gridUrlChange: boolean;
  @Input() isGridFilterOn: boolean;
  @Input() includeHtmlFieldsMap: Map<String, String>;
  @Input() sortField: string;
  @Input() sortDirection: string;
  @Input() advanceFilterProperty: string;
  @Input() websocketUrl: string;
  @Input() subscribeToWebsocket: string;
  @Output() selectedItemsChange = new EventEmitter();
  @Output() clickableLinksClkHandler = new EventEmitter();
  @Output() dataFromComponentHandler = new EventEmitter();
  @Output() manipulateGridData: EventEmitter<any> = new EventEmitter();  //need to use this callback method to update grid using websocket

  displayedColumns = [];
  selectedCheckBox = {};
  selectedAllCheckBox = false;
  allowFiltering = true;
  dataSource: FabircGridDataSource;
  openGridFiltersFields = false;
  advanceFields = [];
  menuContentAdvanceFilter = [];
  showplaceholderFilter  =  [];
  private stompClient;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private restangular: Restangular, private httpClient: HttpClient, private cd: ChangeDetectorRef, private zone: NgZone) {
    iconRegistry.addSvgIcon('edit', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/dc-google-Edit.svg'));
    iconRegistry.addSvgIcon('delete', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/ic_delete_black_18px.svg'));
    iconRegistry.addSvgIcon('clearAll', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/ic_clear_all_black_18px.svg'));
    iconRegistry.addSvgIcon('selectedItem', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/ic_select_all_black_18px.svg'));
    iconRegistry.addSvgIcon('filter', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/ic_filter_list_white_24px.svg'));
    iconRegistry.addSvgIcon('add', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/dc-google-pams-add.svg'));
    iconRegistry.addSvgIcon('visible', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/ic_visibility_black_18px.svg'));
    iconRegistry.addSvgIcon('delete_forever', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/dc-google-delete-forever.svg'));
    iconRegistry.addSvgIcon('moveOut', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/dc-moveOut.svg'));
    iconRegistry.addSvgIcon('view', sanitizer.bypassSecurityTrustResourceUrl('assets/svgs/view.svg'));

    this.resultProperty = "results";
    this.resultTotalCountProperty = "totalCount";
    this.firstResultProperty = "$skip";
    this.fetchSizeProperty = "$top";
    this.advanceFilterProperty = "$filter";
    this.paginationLimitOptions = [5, 10, 25, 100];
    this.pageLimit = 5;
    this.initialPage = 0;
    this.enableRowSelection = false;
    this.singleSelect = false;
    this.isGridFilterOn = true;
    this.gridColumnHeaders = [];
    this.customColumns = [];
    this.topActionButtons = [];
    this.sortField = 'sortField';
    this.sortDirection = 'sortDirection';
  }
  ngOnInit() {
    this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, null);

    if (this.websocketUrl) {
      this.initializeWebSocketConnection(this);
    }
    if (this.gridColumnUrl) {
      this.makeGridHeaders();
    } else {
      this.makeTableColumns();
    }

    this.setAdvanceFiltersMenu();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.restUrlChange != undefined && changes.restUrlChange.currentValue != undefined) {
      this.paginator.pageIndex = 0;
      this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, this.likeClauseAdvanceFields);

      this.cd.detectChanges();
    }
    if (changes.updateGridData != undefined && changes.updateGridData.currentValue != undefined) {
      this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, this.likeClauseAdvanceFields);

      this.cd.detectChanges();
    }
    if (changes.gridUrlChange != undefined && changes.gridUrlChange.currentValue != undefined) {
      this.paginator.pageIndex = 0;
      this.makeGridHeaders();
      this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, this.likeClauseAdvanceFields);

      this.cd.detectChanges();
    }

  }
  makeGridHeaders() {
    this.restangular.one(this.gridColumnUrl).get().subscribe(result => {
      this.gridColumnHeaders = result.plain();
      this.makeTableColumns();
    })
  }
  makeTableColumns() {
    if (this.gridColumnHeaders && this.gridColumnHeaders.length > 0 && this.dynamicCustomColumns && this.dynamicCustomColumns.length > 0) {
      for (let i = 0; i < this.gridColumnHeaders.length; i++) {
        for (let j = 0; j < this.dynamicCustomColumns.length; j++) {
          if (this.dynamicCustomColumns[j].title == this.gridColumnHeaders[i].mappingDataField) {
            this.gridColumnHeaders[i]['includeComponent'] = this.dynamicCustomColumns[j].includeComponent;
            if (this.dynamicCustomColumns[j].showGridFilter === false) {
              this.gridColumnHeaders[i].isAdvanceField = false;
            }
          }
        }
      }
    }
    this.displayedColumns = this.gridColumnHeaders.map(x => x.mappingDataField);
    if (this.customColumns && this.customColumns.length > 0) {
      for (let i = 0; i < this.customColumns.length; i++) {
        if (this.customColumns[i].sequence != undefined) {
          this.displayedColumns.splice(this.customColumns[i].sequence, 0, this.customColumns[i].title);
        } else {
          this.displayedColumns.push(this.customColumns[i].title);
        }
      }
    }

    if (this.isGridFilterOn && this.gridColumnHeaders.length > 0) {
      this.displayedColumns.push("filter");
      for (let i = 0; i < this.gridColumnHeaders.length; i++) {
        this.showplaceholderFilter[this.gridColumnHeaders[i].mappingDataField] = "Starts With";
      }
    }
    if (this.enableRowSelection) {
      this.displayedColumns.splice(0, 0, "select");
    }
  }
  emitSelectedData() {
    this.selectedItemsChange.emit(this.selectedItems);
  }
  clearAllSelected() {
    this.selectedItems = [];
    this.selectedCheckBox = {};
  }
  rowCheckBoxClicked(rowData, selected) {
    if (this.singleSelect) {
      if (!selected) {
        this.selectedItems = [];
        this.selectedCheckBox = {};
        this.selectedItems[0] = rowData;
      } else {
        this.selectedItems = [];
        this.selectedCheckBox = {};
      }
    } else {
      if (!selected) {
        this.selectedItems[this.selectedItems.length] = rowData;
      } else {
        for (let itr = 0; itr < this.selectedItems.length; itr++) {
          if (this.selectedItems[itr].id == rowData.id) {
            this.selectedItems.splice(itr, 1);
            break;
          }
        }
      }
      this.selectDeselectHeaderCheckBox();
    }
    this.emitSelectedData();
  }
  selectDeselectHeaderCheckBox() {
    var count = 0;
    for (let dataItr = 0; dataItr < this.dataSource.renderedData.length; dataItr++) {
      if (this.selectedItems.includes(this.dataSource.renderedData[dataItr])) {
        count++;
      }
    }

    if (count == this.dataSource.renderedData.length) {
      this.selectedAllCheckBox = true;
    } else {
      this.selectedAllCheckBox = false;
    }
  }
  allCheckBoxClicked(selected) {
    if (!selected) {
      for (let itr = 0; itr < this.dataSource.renderedData.length; itr++) {
        var containsRecord = false;
        for (let selectItr = 0; selectItr < this.selectedItems.length; selectItr++) {
          if (this.dataSource.renderedData[itr].id == this.selectedItems[selectItr].id) {
            containsRecord = true;
            break;
          }
        }
        if (!containsRecord) {
          this.selectedItems[this.selectedItems.length] = this.dataSource.renderedData[itr];
          this.selectedCheckBox[this.dataSource.renderedData[itr].id] = true;
        }
      }
    } else {

      for (let itr = 0; itr < this.dataSource.renderedData.length; itr++) {
        for (let selectItr = 0; selectItr < this.selectedItems.length; selectItr++) {
          if ((this.dataSource.renderedData[itr].id) == (this.selectedItems[selectItr].id)) {
            this.selectedCheckBox[this.selectedItems[selectItr].id] = false;
            this.selectedItems.splice(selectItr, 1);
            break;
          }
        }
      }
    }
    this.emitSelectedData();
  }

  clickableCellClkHandler(rowData, columnData) {
    this.clickableLinksClkHandler.emit({ row: rowData, column: columnData });
  }
  dataFromComponent(data) {
    if (Object.keys(data).length > 0) {
      this.dataFromComponentHandler.emit(data);
    }
  }

  setAdvanceFiltersMenu() {
    this.menuContentAdvanceFilter = [
      {
        menuItem: 'alnt.grid.filter.menu.clear',
        className: 'ae-grid-filter-clear',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'Start';
          this.advanceFields[mappingField] = '';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.startswith";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.startswith',
        className: 'ae-grid-filter-startsWith',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'Start';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.startswith";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.endswith',
        className: 'ae-grid-filter-endsWith',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'End';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.endswith";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.equals',
        className: 'ae-grid-filter-equals',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'Equal';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.equals";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.contains',
        className: 'ae-grid-filter-contains',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'Contains';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.contains";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.notequals',
        className: 'ae-grid-filter-notEquals',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'NotEqual';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.notequals";
        }
      },
      {
        menuItem: 'alnt.grid.filter.menu.notcontains',
        className: 'ae-grid-filter-notContains',
        btnClckHandler: (mappingField) => {
          this.advanceFieldsChoice[mappingField] = 'NotContains';
          this.advanceFilterSearch(0);
          this.showplaceholderFilter[mappingField] = "alnt.grid.filter.menu.notcontains";
        }
      }
    ]
  }

  advanceFieldsChoice = [];
  likeClauseAdvanceFields: string;
  advanceFilterSearch(event) {
    this.likeClauseAdvanceFields = '';
    for (var itrFilter = 0; itrFilter < this.gridColumnHeaders.length; itrFilter++) {
      if (this.gridColumnHeaders[itrFilter].isAdvanceField == true) {
        var filterValue = this.advanceFieldsChoice[this.gridColumnHeaders[itrFilter].mappingDataField];
        if (!filterValue) {
          filterValue = 'Start';
        }
        var mappingFieldName = this.gridColumnHeaders[itrFilter].mappingDataField;
        var filterKey = this.gridColumnHeaders[itrFilter].mappingDataField;
        if (this.advanceFields[filterKey] == null || this.advanceFields[filterKey] == undefined || this.advanceFields[filterKey].trim() == '' || this.advanceFields[filterKey].length < 1 || this.advanceFields[filterKey] === '*') {
          continue;
        } else if (filterValue == 'Start') {
          this.likeClauseAdvanceFields += "startswith(" + mappingFieldName + ",'" + this.advanceFields[filterKey] + "') eq true";
        } else if (filterValue == 'End') {
          this.likeClauseAdvanceFields += "endswith(" + mappingFieldName + ",'" + this.advanceFields[filterKey] + "') eq true";
        } else if (filterValue == 'Equal') {
          this.likeClauseAdvanceFields += mappingFieldName + " eq '" + this.advanceFields[filterKey] + "' ";
        } else if (filterValue == 'Contains') {
          this.likeClauseAdvanceFields += "indexof(" + mappingFieldName + ",'" + this.advanceFields[filterKey] + "') ge 0";
        } else if (filterValue == 'NotEqual') {
          this.likeClauseAdvanceFields += mappingFieldName + " ne '" + this.advanceFields[filterKey] + "' ";
        } else if (filterValue == 'NotContains') {
          this.likeClauseAdvanceFields += "indexof(" + mappingFieldName + ",'" + this.advanceFields[filterKey] + "') ge -1";
        }
        this.likeClauseAdvanceFields += " and ";
      }
    }
    this.likeClauseAdvanceFields = this.likeClauseAdvanceFields.substring(0, this.likeClauseAdvanceFields.length - 4);
    // console.log(this.likeClauseAdvanceFields);

    //if(this.likeClauseAdvanceFields.trim()!=''){
    this.paginator.pageIndex = 0;
    this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, this.likeClauseAdvanceFields);


  }
  gridFilterClicked() {
    this.openGridFiltersFields = !this.openGridFiltersFields;
    if (!this.openGridFiltersFields) {
      this.likeClauseAdvanceFields = null;
      this.advanceFields = [];
      this.advanceFieldsChoice = [];
      this.dataSource = new FabircGridDataSource(this.paginator, this.sort, this.restUrl, this.staticData, this.resultProperty, this.resultTotalCountProperty, this.firstResultProperty, this.fetchSizeProperty, this.httpClient, this.isGridFilterOn, this.sortField, this.sortDirection, this.restangular, this.advanceFilterProperty, null);

    }
  }
  initializeWebSocketConnection(parentRef) {
    /*
    let ws = new SockJS(this.websocketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe(parentRef.subscribeToWebsocket, (message) => {
        if (parentRef.dataSource) {
          let data = parentRef.dataSource.renderedData;
          let updateData = JSON.parse(message.body);
          parentRef.manipulateGridData.emit({ "gridData": data, "websocketData": updateData });
          parentRef.dataSource.renderedData = data;
        }
      });
    });*/
  }
}