import { Inject } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

export class MatPaginatorIntlLabel extends MatPaginatorIntl {
    itemsPerPageLabel: string;
    nextPageLabel: string;
    previousPageLabel: string;

    constructor() {
        super();
        this.itemsPerPageLabel = "Items per page";
        this.nextPageLabel = "Next page";
        this.previousPageLabel = "Previous page";
    }
}