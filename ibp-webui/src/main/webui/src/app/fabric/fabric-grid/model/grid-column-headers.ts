export class GridColumnHeaders {
    title: string;
    mappingDataField: string;
    isDate: boolean;
    isAdvanceField: boolean;

    constructor(title: string, mappingDataField: string, isDate: boolean, isAdvanceField: boolean) {
        this.title = title;
        this.mappingDataField = mappingDataField;
        this.isDate = isDate;
        this.isAdvanceField = !!isAdvanceField;
    }
}