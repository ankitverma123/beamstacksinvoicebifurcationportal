import { Inject } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator,MatSort } from '@angular/material';
import { Restangular } from 'ngx-restangular';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/merge';
import { HttpClient,HttpParams  } from '@angular/common/http';
import 'rxjs/add/operator/startWith';
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';

export class FabircGridDataSource extends DataSource<any>{
    renderedData = [];
    params = {};
    pageChange;
    pageSizeChange;
    totalCount = 0;

    constructor(private _paginator: MatPaginator,
        private _sort: MatSort,
        private _restUrl,
        private _staticData,
        private _resultProperty,
        private _resultTotalCountProperty,
        private _firstResultProperty,
        private _fetchSizeProperty,
        private httpClient: HttpClient,
        private _isGridFilterOn,
        private _sortField,
        private _sortDirection,
        private restangular: Restangular,
        private _$filterProperty,
        private _$filter
    ) {

        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any> {

        const displayDataChanges = [
            this._paginator.page,
            this._sort.sortChange
        ];

        if (this._restUrl) {
            return Observable.merge(...displayDataChanges).startWith(null).switchMap(() => {

                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                this.params[this._firstResultProperty] = startIndex;
                this.params[this._fetchSizeProperty] = this._paginator.pageSize;
                if (this._$filter) {
                    this.params[this._$filterProperty] = this._$filter;
                }
                if (this._sort.active) {
                    this.params[this._sortField] = this._sort.active;
                    this.params[this._sortDirection] = this._sort.direction;
                }
                return this.restangular.one(this._restUrl).get(this.params);
            }).map(result => {
                this.renderedData = result[this._resultProperty];
                this.totalCount = result[this._resultTotalCountProperty];
                this.pageChange = this._paginator._pageIndex;
                this.pageSizeChange = this._paginator.pageSize;
                if (this._isGridFilterOn) {
                    this.renderedData.splice(0, 0, {});
                }

                return this.renderedData;

            });
        }

        if (this._staticData) {
            return Observable.merge(...displayDataChanges).startWith(null).switchMap(() => {
                return Observable.of(this.renderedData);
            }).map(result => {
                const _staticData = _.reject(this._staticData, _.isEmpty);
                this._staticData = _staticData;
                this.renderedData = this._staticData;
                this.totalCount = this._staticData.length;
                this.pageChange = this._paginator._pageIndex;
                this.pageSizeChange = this._paginator.pageSize;
                if (this._isGridFilterOn) {
                    this.renderedData.splice(0, 0, {});
                }
                this.renderedData = this.renderedData.slice(this._paginator.pageIndex * this._paginator.pageSize, this.pageSizeChange + (this._paginator.pageIndex * this._paginator.pageSize));

                return this.renderedData;
            });
        }
    }
    disconnect() { }

    getSortedData() {
        const data = this.renderedData;
        if (this._isGridFilterOn) {
            data.splice(0, 1);  // done this for grid filters
        }

        data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._sort.active) {
                case this._sort.active: [propertyA, propertyB] = [a[this._sort.active], b[this._sort.active]]; break;
            }
            let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
        });
        if (this._isGridFilterOn) {
            data.splice(0, 0, {});  // done this for grid filters        
        }
        return data;
    }
}
