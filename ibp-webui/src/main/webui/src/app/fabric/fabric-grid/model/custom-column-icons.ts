export class CustomColumnIcons {
    iconName: string;
    isDisabled: Boolean;
    tooltip: string;
    customColumnClkHandler: Function;

    constructor(iconName: string, isDisabled: Boolean, tooltip: string, customColumnClkHandler: Function) {
        this.iconName = iconName;
        this.isDisabled = isDisabled;
        this.tooltip = tooltip;
        this.customColumnClkHandler = customColumnClkHandler;
    }
}