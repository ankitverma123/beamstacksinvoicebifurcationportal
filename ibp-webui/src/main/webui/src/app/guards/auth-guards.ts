import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "../_services/auth/authentication.service";
import { TokenStorage } from "./TokenStorage";

@Injectable()
export class AuthGuards implements CanActivate{

    constructor(private authService: AuthenticationService, private router: Router, private route: ActivatedRoute, private tokenStorage: TokenStorage){}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
      if ( this.tokenStorage.getToken() != null) {
        return true;
      } else {
        this.router.navigate(['/login']);
          return false;
      }
    }

}
