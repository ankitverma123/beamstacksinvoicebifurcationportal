import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { TokenStorage } from "./guards/TokenStorage";

@Injectable()
export class HttpCalIInterceptor implements HttpInterceptor {
    constructor(private injector: Injector, private tokenStorage: TokenStorage) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {   
        const token: string = this.tokenStorage.getToken();

        if(token){
            request = request.clone({ headers: request.headers.set('Authorization', token) });
        }
        
        console.log('Intercepted request= ' + request.url);
        return next.handle(request);
    }
}
