import { Component, Input, OnInit } from '@angular/core';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements OnInit {
  title = 'webui';
  events: string[] = [];
  opened: boolean;
  
  navigation;

  @Input() isVisible : boolean = true;
  talkBack(e: string) {
    this.opened = !this.opened;    
  }

  constructor(private media: ObservableMedia) {}

  

  ngOnInit() {}

 
}
