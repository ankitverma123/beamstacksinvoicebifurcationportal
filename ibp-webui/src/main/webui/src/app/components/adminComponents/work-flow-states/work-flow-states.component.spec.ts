import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkFlowStatesComponent } from './work-flow-states.component';

describe('WorkFlowStatesComponent', () => {
  let component: WorkFlowStatesComponent;
  let fixture: ComponentFixture<WorkFlowStatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkFlowStatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkFlowStatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
