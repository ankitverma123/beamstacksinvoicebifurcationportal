import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommonService } from 'src/app/_services/common/common.service';
import { AppConstants } from 'src/app/app-constants';

@Component({
  selector: 'app-bank-manager',
  templateUrl: './bank-manager.component.html',
  styleUrls: ['./bank-manager.component.css']
})
export class BankManagerComponent implements OnInit {
  bankForm = this.fb.group({
    'id': [this.data.id],
    'name': [this.data.label],
  });
  title = "";
  constructor(private commonService: CommonService, 
    private fb: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BankManagerComponent>) {
    this.title = data.id === -1 ? 'New Bank' : 'Modify Bank';
   }

  ngOnInit() {
  }

  onSubmitBank() {
    this.commonService.postCall(AppConstants.BANK_CREAT_UPDATE_REST_END_POINT, this.bankForm.value).subscribe(response => {
      console.log(response);
      this.dialogRef.close({reload: true});
    });
  }

  close() {
    this.dialogRef.close({reload: false});
  }

}
