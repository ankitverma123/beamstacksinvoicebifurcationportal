import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { AppConstants } from 'src/app/app-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-local-users',
  templateUrl: './local-users.component.html',
  styleUrls: ['./local-users.component.css']
})
export class LocalUsersComponent implements OnInit {
  gridToBeRefresh: Subject<any> = new Subject();
  frameworkComponents: any;
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  pageBarButtons = [
    {
      type: 'iconButton',
      icon: 'add',
      refrence: this,
      btnClkHandler: this.addNewUserBtnClkHandler
    }
  ];

  gridUrl = AppConstants.LOCAL_USERS_GRID_REST_END_POINT;
  columnDefinitions = [
    { headerName: 'First Name', field: 'firstName', filter: false },
    { headerName: 'Last Name', field: 'lastName', filter: false },
    { headerName: 'Username', field: 'userName', filter: false },
    { headerName: 'Email', field: 'email', filter: false },
    { headerName: 'Bank', field: 'bank', filter: false },
    { headerName: 'ICA', field: 'ica', filter: false },
    { headerName: 'Role', field: 'roles', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '100',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
        <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">edit</span>
          `;
      }
    }
  ];
  staticData = []

  onAction(e: any) {

  }

  addNewUserBtnClkHandler(this) {
    console.log('Add new User Btn Clk Handler');
    this.refrence.router.navigate(['/createModifyUser']);
  }

}
