import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create-modify-user',
  templateUrl: './create-modify-user.component.html',
  styleUrls: ['./create-modify-user.component.css']
})
export class CreateModifyUserComponent implements OnInit {
  localUserForm = this.fb.group({
    'id': [null],
    'firstName': [''],
    'lastName': [''],
    'userName': [''],
    'password': [''],
    'email': [''],
    'bank': ['-1'],
    'role': ['-1'],
    'allocatedIcas': ['-1']
  });
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

}
