import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from 'src/app/app-constants';
import * as _ from 'lodash';
import { CommonService } from 'src/app/_services/common/common.service';

@Component({
  selector: 'app-work-flow-states',
  templateUrl: './work-flow-states.component.html',
  styleUrls: ['./work-flow-states.component.css']
})
export class WorkFlowStatesComponent implements OnInit {
  workFlowStates = []
  constructor(private httpClient: HttpClient, private commonService: CommonService) { }

  ngOnInit() {
    this.httpClient.get(AppConstants.WORK_FLOW_STATES_REST_END_POINT).subscribe(response => {
      this.workFlowStates = response['data'];
    });
  }

  updateWorkFlowStateLabel(itemId: any) {
    let workFlowStateToUpdate = _.find(this.workFlowStates, { 'id': itemId });
    console.log(workFlowStateToUpdate);
    this.commonService.postCall(AppConstants.UPDATE_WORK_FLOW_STATES_REST_END_POINT, workFlowStateToUpdate).subscribe(response => {
      console.log(response);
    });
  }

}
