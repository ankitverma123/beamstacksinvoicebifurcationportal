import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/_services/common/common.service';
import { AppConstants } from 'src/app/app-constants';

@Component({
  selector: 'app-ica-manager',
  templateUrl: './ica-manager.component.html',
  styleUrls: ['./ica-manager.component.css']
})
export class IcaManagerComponent implements OnInit {
  icaForm = this.fb.group({
    'id': [this.data.id],
    'identifier': [this.data.label],
    'bank': [this.data.activeBank]
  });
  title = "";
  constructor(private commonService: CommonService, 
    private fb: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<IcaManagerComponent>) {
    this.title = data.id === -1 ? 'New ICA' : 'Modify ICA';
   }

  ngOnInit() {
  }

  onSubmitBank() {
    this.commonService.postCall(AppConstants.ICA_CREAT_UPDATE_REST_END_POINT, this.icaForm.value).subscribe(response => {
      console.log(response);
      this.dialogRef.close({reload: true});
    });
  }

  close() {
    this.dialogRef.close({reload: false});
  }
}
