import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/_services/common/common.service';
import { AppConstants } from 'src/app/app-constants';
import * as _ from 'lodash';
import { BankManagerComponent } from './bank-manager/bank-manager.component';
import { MatDialog } from '@angular/material';
import { IcaManagerComponent } from './ica-manager/ica-manager.component';

@Component({
  selector: 'app-bank-ica-management',
  templateUrl: './bank-ica-management.component.html',
  styleUrls: ['./bank-ica-management.component.css']
})
export class BankIcaManagementComponent implements OnInit {
  banks = [];
  icas = [];
  activeBank: string = '';
  bankName: string = ''
  bankLoading: boolean = false;
  icaLoading: boolean = false;

  constructor(private commonService: CommonService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.loadBanks();
  }

  loadBanks() {
    this.bankLoading = true;
    this.commonService.getCall(AppConstants.BANK_DROPDOWN_REST_END_POINT).subscribe(response => {
      this.bankLoading = false;
      this.banks = response.data;
      this.loadBankIca(this.banks[0].id);
    });
  }

  loadBankIca(bankUUID: any) {
    this.activeBank = bankUUID;
    const bankDetails = _.find(this.banks, { id: bankUUID });
    this.bankName = bankDetails ? `${bankDetails.label} ICA Management` : 'ICA Management';
    this.icaLoading = true;
    this.commonService.getCall(`${AppConstants.BANK_ICA_REST_END_POINT}${bankUUID}`).subscribe(response => {
      this.icaLoading = false;
      this.icas = response.data;
    });
  }

  bankManager(bankUUID: any) {
    let bankDetails = _.find(this.banks, { id: bankUUID });
    if (!bankDetails) {
      bankDetails = { id: -1, label: '' }
    }
    const dialogRef = this.dialog.open(BankManagerComponent, {
      width: '500px',
      data: bankDetails
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.reload) {
        this.loadBanks();
      }
    });
  }

  icaManager(uuid: any) {
    let icaDetails = _.find(this.icas, { id: uuid });
    if (!icaDetails) {
      icaDetails = { id: -1, identifier: '', activeBank: this.activeBank }
    } else {
      icaDetails['activeBank'] = this.activeBank;
    }
    const dialogRef = this.dialog.open(IcaManagerComponent, {
      width: '500px',
      data: icaDetails
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.reload) {
        this.loadBanks();
      }
    });
  }

}
