import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcaManagerComponent } from './ica-manager.component';

describe('IcaManagerComponent', () => {
  let component: IcaManagerComponent;
  let fixture: ComponentFixture<IcaManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcaManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcaManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
