import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankIcaManagementComponent } from './bank-ica-management.component';

describe('BankIcaManagementComponent', () => {
  let component: BankIcaManagementComponent;
  let fixture: ComponentFixture<BankIcaManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankIcaManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankIcaManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
