import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateModifyUserComponent } from './create-modify-user.component';

describe('CreateModifyUserComponent', () => {
  let component: CreateModifyUserComponent;
  let fixture: ComponentFixture<CreateModifyUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateModifyUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateModifyUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
