import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoveryDashboardComponent } from './discovery-dashboard.component';

describe('DiscoveryDashboardComponent', () => {
  let component: DiscoveryDashboardComponent;
  let fixture: ComponentFixture<DiscoveryDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscoveryDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscoveryDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
