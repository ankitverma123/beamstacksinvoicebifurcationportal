import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as echarts from 'echarts';
import { DashboardService } from '../../../_services/dashboard/dashboard.service';


@Component({
    selector: 'app-discovery-dashboard',
    templateUrl: './discovery-dashboard.component.html',
    styleUrls: ['./discovery-dashboard.component.css']
})
export class DiscoveryDashboardComponent implements OnInit {
    @ViewChild('myDiv') myDiv: ElementRef;

    @Input() legendShow: boolean;
    @Input() heading: string;
    @Input() position;
    @Input() height: string;
    @Input() radius;
    noMembers = 0;
    noOwners = 0;
    inActiveOwners = 0;
    dormant = 0;
    option: any;

    data = [
        { value: this.noMembers, name: 'No members' },
        { value: this.noOwners, name: 'No owners' },
        { value: this.inActiveOwners, name: 'Inactive owners' },
        { value: this.dormant, name: 'Dormant Groups' }
    ];
    constructor(private dashBoardService: DashboardService) {
     }

    ngOnInit() {
        this.myDiv.nativeElement.style.height = this.height;
        const myChart = echarts.init(this.myDiv.nativeElement);
        this.option = {
            title: {
                text: this.heading,
                subtext: this.heading,
            },
            color: ['#3398DB'],
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Axis', 'Citi', 'HDFC', 'SBI', 'RBI', 'Kotak', 'Amex'],
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: 'Bank Invoices',
                    type: 'bar',
                    barWidth: '60%',
                    data: [10, 52, 200, 334, 390, 330, 220]
                }
            ]
        };
        myChart.setOption(this.option);

    }

}

