import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/_services/common/common.service';
import { AppConstants } from 'src/app/app-constants';
import { MatDialogRef } from '@angular/material';

@Component({
	selector: 'app-reject-invoice',
	templateUrl: './reject-invoice.component.html',
	styleUrls: ['./reject-invoice.component.css']
})
export class RejectInvoiceComponent implements OnInit {
	rejectReasons = []
	rejectInvoiceForm = this.fb.group({
		'rejectionReason': [''],
		'rejectionNotes': ['']
	});

	constructor(private fb: FormBuilder, private commonService: CommonService, public dialogRef: MatDialogRef<RejectInvoiceComponent>) { }

	ngOnInit() {
		this.commonService.getCall(AppConstants.REJECT_REASONS_REST_END_POINT).subscribe(response => {
			this.rejectReasons = response.data;
		});
	}

	onSubmit() {
		this.dialogRef.close(this.rejectInvoiceForm.value);
	}
}
