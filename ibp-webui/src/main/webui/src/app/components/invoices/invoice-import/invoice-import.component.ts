import { Component, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import {
  UploaderOptions,
  UploadFile,
  UploadInput,
  UploadOutput
} from 'ngx-uploader';
import { TokenStorage } from '../../../guards/TokenStorage';

@Component({
  selector: 'app-invoice-import',
  templateUrl: './invoice-import.component.html',
  styleUrls: ['./invoice-import.component.css']
})
export class InvoiceImportComponent implements OnInit {
  @ViewChild('fileInput', { read: ElementRef }) fileInput: ElementRef;

  fileUploadIsInProgress: boolean = false;
  responseData: any = [];

  options: UploaderOptions;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;

  constructor(private tokenStorage: TokenStorage) {
    this.options = {
      concurrency: 1,
      maxUploads: 16
    };
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>();
  }

  ngOnInit() {
    this.fileInput.nativeElement.onclick = function () {
      this.value = null;
    };
  }

  onUploadOutput(output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        console.log('case allAddedToQueue');
        break;
      case 'addedToQueue':
        console.log('case addedToQueue');
        if (typeof output.file !== 'undefined') {
          this.files.push(output.file);
        }
        break;
      case 'uploading':
        console.log('case uploading');
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.files.findIndex(
            file =>
              typeof output.file !== 'undefined' && file.id === output.file.id
          );
          this.files[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        console.log('case done');
        this.files = this.files.filter(
          (file: UploadFile) => file !== output.file
        );
        break;
      case 'done':
        // The file is downloaded
        this.fileUploadIsInProgress = false;
        console.log('case done');
        const fileUploadResponse = output.file.response;
        this.responseData = fileUploadResponse.data;
        this.removeFile(this.files[0].id);
    }
  }

  startUpload(): void {
    this.fileUploadIsInProgress = true;
    const event: UploadInput = {
      type: 'uploadAll',
      url: '/rest/invoices/import',
      method: 'POST',
      headers: {
        Authorization: this.tokenStorage.getToken()
      }
    };
    this.uploadInput.emit(event);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }


}
