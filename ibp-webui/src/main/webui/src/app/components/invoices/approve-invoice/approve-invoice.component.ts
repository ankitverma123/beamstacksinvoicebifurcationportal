import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-approve-invoice',
  templateUrl: './approve-invoice.component.html',
  styleUrls: ['./approve-invoice.component.css']
})
export class ApproveInvoiceComponent implements OnInit {
  approveInvoiceForm = this.fb.group({
    'approvalNotes': ['']
  });
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<ApproveInvoiceComponent>) { }

  ngOnInit() {
  }

  onSubmit() {
    this.dialogRef.close(this.approveInvoiceForm.value);
  }

}
