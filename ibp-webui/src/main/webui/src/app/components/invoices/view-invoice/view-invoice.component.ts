import { Component, OnInit, Inject } from '@angular/core';
import { CommonService } from 'src/app/_services/common/common.service';
import { AppConstants } from 'src/app/app-constants';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.css']
})
export class ViewInvoiceComponent implements OnInit {
  auditLog: any = []
  invoiceDetails: any;
  constructor(private commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ViewInvoiceComponent>) { }

  ngOnInit() {
    const url = `${AppConstants.INVOICES_AUDIT_LOG_REST_END_POINT}${this.data.invoiceId}`;
    this.commonService.getCall(url).subscribe(response => {
      this.auditLog = response.data;   
    });

    const invoiceDetailsUrl = `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=id_eq_${this.data.invoiceId}&pageNumber=1&pageSize=1`;
    this.commonService.getCall(invoiceDetailsUrl).subscribe(response => {
      this.invoiceDetails = response.data.results[0];
    });
  }

}
