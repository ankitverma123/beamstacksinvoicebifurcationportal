import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { InvoiceImportComponent } from './invoice-import/invoice-import.component';
import { AppConstants } from '../../app-constants';
import { CommonService } from '../../_services/common/common.service';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import { RejectInvoiceComponent } from './reject-invoice/reject-invoice.component';
import { ApproveInvoiceComponent } from './approve-invoice/approve-invoice.component';
import { ViewInvoiceComponent } from './view-invoice/view-invoice.component';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  gridToBeRefresh: Subject<any> = new Subject();
  gridToBeRefreshConstant: Subject<any> = new Subject();

  activatedTab = 0;

  frameworkComponents: any;
  dynamicTabs = [];
  activeRole = null;

  allPotentialRoles = [AppConstants.ROLE_AGENT, AppConstants.ROLE_ISSUER, AppConstants.ROLE_ACQUIRER]

  columnDefinitionsAgentSent = [
    { checkboxSelection: true, sortable: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { sortable: true, headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider Bank Name', field: 'issuerBankName', filter: false },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { sortable: true, headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Status', field: 'workFlowStateStr', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '150',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
        <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
        <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
        `;
      }
    }
  ];

  columnDefinitionsAgentRejected = [
    { checkboxSelection: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider Bank Name', field: 'issuerBankName', filter: false },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Rejected By', field: 'rejectedBy', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '225',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
        <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
        <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
        <span style="padding: 3px; cursor: pointer;" data-action="sendInvoice" class="material-icons">near_me</span>
        `;
      }
    }
  ];

  columnDefinitionsIssuerInComing = [
    { checkboxSelection: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Status', field: 'workFlowStateStr', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '200',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
          <span style="padding: 3px; cursor: pointer;" data-action="approveInvoice" class="material-icons">thumb_up</span>
          <span style="padding: 3px; cursor: pointer;" data-action="rejectInvoice" class="material-icons">thumb_down</span>
          <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
          <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
          `;
      }
    }
  ];

  columnDefinitionsIssuerApproved = [
    { checkboxSelection: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Status', field: 'workFlowStateStr', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '150',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
          <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
          <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
          <span style="padding: 3px; cursor: pointer;" data-action="uploadInvoice" class="material-icons">cloud_upload</span>
          `;
      }
    }
  ];

  columnDefinitionsIssuerRejected = [
    { checkboxSelection: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Rejected By', field: 'rejectedBy', filter: false },
    { headerName: 'Status', field: 'workFlowStateStr', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '150',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
          <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
          <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
          <span style="padding: 3px; cursor: pointer;" data-action="sendInvoice" class="material-icons">near_me</span>
          `;
      }
    }
  ];

  columnDefinitionsIssuerSent = [
    { checkboxSelection: true, headerName: 'Invoice No.', field: 'invoiceNumber', filter: false },
    { headerName: 'Invoice Date', field: 'invoiceDate', filter: false, valueFormatter: (param) => (param.value ? this.dateFormatter(param.value) : '') },
    { headerName: 'Provider ICA', field: 'issuerICAIdentifier', filter: false },
    { headerName: 'Receiver Bank Name', field: 'acquirerBankName', filter: false },
    { headerName: 'Receiver ICA', field: 'acquirerICAIdentifier', filter: false },
    { headerName: 'Total Invoice Amount', field: 'totaInvoiceAmt', filter: false },
    { headerName: 'Status', field: 'workFlowState', filter: false },
    {
      headerName: 'Actions', field: 'action', pinned: 'right', filter: false, width: '100',
      onCellClicked: this.onAction.bind(this), cellRenderer: (param: any) => {
        return `
          <span style="padding: 3px; cursor: pointer;" data-action="viewInvoice" class="material-icons">remove_red_eye</span>
          <span style="padding: 3px; cursor: pointer;" data-action="downloadInvoice" class="material-icons">cloud_download</span>
          `;
      }
    }
  ];


  dateFormatter(dateValue) {
    const date = new Date(dateValue);
    const formattedDate = date.toLocaleDateString('en-GB', {
      day: '2-digit', month: 'short', year: 'numeric'
    }).replace(/ /g, '-');
    return formattedDate;
  }

  onAction(e: any) {
    const action = e.event.target.dataset.action;
    if (action === 'viewInvoice') {
      this.viewInvoice(e.data.id);
    } else if (action === 'downloadInvoice') {
      console.log('DownloadClkHandler ' + e.data);
    } else if (action === 'uploadInvoice') {
      console.log('UploadClkHandler ' + e.data);
    } else if (action === 'sendInvoice') {
      console.log('SendInvoiceClkHandler ' + e.data);
    } else if (action === 'approveInvoice') {
      this.approveInvoiceHandler(e.data.id);
    } else if (action === 'rejectInvoice') {
      this.rejectInvoiceHandler(e.data.id);
    }
  }

  tabHeaderColor = 'primary';
  bankNames = []

  providerIcaList = []
  receiverIcaList = []

  filterForm = this.fb.group({
    'invoiceNumber': [''],
    'issuer.bankID.id': ['-1'],
    'issuer.id': ['-1'],
    'acquirer.bankID.id': ['-1'],
    'acquirer.id': ['-1']
  });

  constructor(public dialog: MatDialog,
    private commonService: CommonService,
    private fb: FormBuilder) {
    const loggedInUserRole = commonService.loggedInUserRole();
    this.activeRole = loggedInUserRole;
    if (loggedInUserRole.includes(AppConstants.ROLE_AGENT)) {
      this.dynamicTabs.push(
        {
          title: 'Sent',
          gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=fileTrackingId.uploadedBy.id_eq_${AppConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID},workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_SENT_BY_AGENT}`,
          columnDefinitions: this.columnDefinitionsAgentSent,
          staticData: null,
          button: [{
            label: 'Add Invoice',
            class: 'btn btn-sm btn-orange',
            refrence: this,
            btnClkHandler: this.openFileUploadDialog
          }]
        }, {
        title: 'Rejected By Provider',
        gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=fileTrackingId.uploadedBy.id_eq_${AppConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID},workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_REJECTED_BY_ISSUER}`,
        columnDefinitions: this.columnDefinitionsAgentRejected,
        staticData: null,
        button: [{
          label: 'Add Invoice',
          class: 'btn btn-sm btn-orange',
          refrence: this,
          btnClkHandler: this.openFileUploadDialog
        }]
      }, {
        title: 'Rejected By Receiver',
        gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=fileTrackingId.uploadedBy.id_eq_${AppConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID},workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_REJECTED_BY_ACQUIRER}`,
        columnDefinitions: this.columnDefinitionsAgentRejected,
        staticData: null,
        button: [{
          label: 'Add Invoice',
          refrence: this,
          class: 'btn btn-sm btn-orange',
          btnClkHandler: this.openFileUploadDialog
        }]
      }
      );
    } else if (loggedInUserRole.includes(AppConstants.ROLE_ISSUER)) {
      this.dynamicTabs.push(
        {
          title: 'Incoming',
          gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_SENT_BY_AGENT},issuer.bank.id_eq_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK},issuer.id_in_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA}`,
          columnDefinitions: this.columnDefinitionsIssuerInComing,
          staticData: null,
          button: [{
            label: 'Approve',
            class: 'btn btn-sm btn-orange',
            refrence: this,
            btnClkHandler: this.openFileUploadDialog
          }]
        }, {
        title: 'Approved',
        gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_APPROVED_BY_ISSUER},issuer.bank.id_eq_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK},issuer.id_in_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA}`,
        columnDefinitions: this.columnDefinitionsIssuerApproved,
        staticData: null,
        button: []
      }, {
        title: 'Rejected',
        gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_REJECTED_BY_ISSUER},issuer.bank.id_eq_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_BANK},issuer.id_in_${AppConstants.SPECIAL_FILTER_NAME_MY_ALLOCATED_ICA}`,
        columnDefinitions: this.columnDefinitionsIssuerRejected,
        staticData: null,
        button: []
      }, {
        title: 'Sent',
        gridUrl: `${AppConstants.INVOICES_GRID_REST_END_POINT}?filter=fileTrackingId.uploadedBy.id_eq_${AppConstants.SPECIAL_FILTER_NAME_LOGGEDIN_USERID},workFlowState.identifier_eq_${AppConstants.WORK_FLOW_STATE_REJECTED_BY_ACQUIRER}`,
        columnDefinitions: this.columnDefinitionsIssuerSent,
        staticData: null,
        button: []
      }
      );
      commonService.getCall(AppConstants.MY_ASSIGNED_ICA_REST_END_POINT).subscribe(response => {
        this.providerIcaList = response.data;
      })
    }

  }

  openFileUploadDialog(this) {
    const dialogRef = this.refrence.dialog.open(InvoiceImportComponent, { width: '500px' });
    dialogRef.afterClosed().subscribe(result => {
      this.refrence.gridToBeRefresh.next({ reload: true });
    });
  }

  ngOnInit() {
    this.commonService.getCall(AppConstants.BANK_DROPDOWN_REST_END_POINT).subscribe(response => {
      this.bankNames = response.data;
    });
  }

  onProviderBankNameChange(value) {
    this.commonService.getCall(AppConstants.BANK_ICA_REST_END_POINT + value).subscribe(response => {
      this.providerIcaList = response.data;
    });
  }

  onReceiverBankNameChange(value: any) {
    this.commonService.getCall(AppConstants.BANK_ICA_REST_END_POINT + value).subscribe(response => {
      this.receiverIcaList = response.data;
    });
  }

  approveInvoiceHandler(invoiceId: any) {
    const dialogRef = this.dialog.open(ApproveInvoiceComponent, { width: '500px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const payload = {
          approvalNotes: result.approvalNotes,
          invoice: invoiceId,
          workFloWState: this.activeRole.includes(AppConstants.ROLE_ISSUER) ? AppConstants.WORK_FLOW_STATE_APPROVED_BY_ISSUER : AppConstants.WORK_FLOW_STATE_APPROVED_BY_ACQUIRER
        }
        this.commonService.postCall(AppConstants.APPROVE_INVOICE_REST_END_POINT, payload).subscribe(response => {
          this.gridToBeRefresh.next({ reload: true });
        });
      }
    });
  }

  rejectInvoiceHandler(invoiceId: any) {
    const dialogRef = this.dialog.open(RejectInvoiceComponent, { width: '500px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const payload = {
          rejectReason: result.rejectionReason,
          rejectionNote: result.rejectionNotes,
          invoice: invoiceId,
          workFloWState: this.activeRole.includes(AppConstants.ROLE_ISSUER) ? AppConstants.WORK_FLOW_STATE_REJECTED_BY_ISSUER : AppConstants.WORK_FLOW_STATE_REJECTED_BY_ACQUIRER
        }
        this.commonService.postCall(AppConstants.REJECT_INVOICE_REST_END_POINT, payload).subscribe(response => {
          this.gridToBeRefresh.next({ reload: true });
        });
      }
    });
  }

  columnShowHideEvaluator(toBeChecked) {
    return _.intersection(this.activeRole, toBeChecked).length ? true : false;
  }

  onSubmitFilters() {
    let filtersForGrid = []
    const formValue = this.filterForm.value;
    for (var key in formValue) {
      if (formValue.hasOwnProperty(key)) {
        const value = formValue[key];
        if (value.length && value != -1 && value != '') {
          filtersForGrid.push(key + "_eq_" + value);
        }
      }
    }
    this.gridToBeRefresh.next({ updateGridFilter: filtersForGrid });
  }

  tabChangeEvent(event: any) {
    this.activatedTab = event;
    this.filterForm.reset();
    this.filterForm.patchValue({
      'invoiceNumber': [''],
      'issuer.bankID.id': ['-1'],
      'issuer.id': ['-1'],
      'acquirer.bankID.id': ['-1'],
      'acquirer.id': ['-1']
    });
  }

  viewInvoice(invoiceId: any) {
    const dialogRef = this.dialog.open(ViewInvoiceComponent, { width: '80%', data: { invoiceId: invoiceId } });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
