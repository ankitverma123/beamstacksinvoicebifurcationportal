import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import * as echarts from 'echarts';
import { Restangular } from 'ngx-restangular';
import { DashboardService } from '../../_services/dashboard/dashboard.service';
import { AppConstants } from '../../app-constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  countingDashboard = [{
    total: Math.floor(Math.random() * 100) + 1,
    title: 'Total Invoices Received',
    url: ''
  }, {
    total: Math.floor(Math.random() * 100) + 1,
    title: 'Total Pending Invoices',
    url: ''
  }, {
    total: Math.floor(Math.random() * 100) + 1,
    title: 'Total Approved Invoices',
    url: ''
  }, {
    total: Math.floor(Math.random() * 100) + 1,
    title: 'Total Rejected Invoices',
    url: ''
  }]

  dynamicDashboard = [
    {
      dashboard: {
        name: AppConstants.BANK_SENT_INVOICES_AGENT_DASHBOARD
      }
    }, {
      dashboard: {
        name: AppConstants.BANK_RECEIVED_INVOICES_AGENT_DASHBOARD
      }
    }
  ]

  constructor() { }

  ngOnInit() { }

}
