import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_services/auth/authentication.service';
import { TokenStorage } from '../guards/TokenStorage';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {
  showProgressBar: boolean;
  errorMessage = false;
  constructor(private router: Router,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private tokenStorage: TokenStorage,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      "master_card_logo_icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/mastercard_logo_symbol.svg")
    );
  }

  userForm: FormGroup;
  loginFormData;
  formErrors = {
    'username': '',
    'password': ''
  };
  validationMessages = {
    'email': {
      'required': 'Please enter your username',
      'username': 'please enter your vaild username'
    },
    'password': {
      'required': 'Please enter your password',
      'pattern': 'The password must contain numbers and letters',
      'minlength': 'Please enter more than 4 characters',
      'maxlength': 'Please enter less than 25 characters',
    }
  };

  buildForm() {
    this.userForm = this.fb.group({
      'username': ['', [
        Validators.required,
      ]
      ],
      'password': ['', [
        Validators.minLength(6),
        Validators.maxLength(25)
      ]
      ],
    });

    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    this.loginFormData = data;
  }
  ngOnInit() {
    this.buildForm();
  }

  login() {
    this.showProgressBar = true;
    this.errorMessage = false;
    this.authenticationService.login(this.loginFormData).subscribe((result: any)=>{
      if(result && result.token){
        this.showProgressBar = false;
        this.tokenStorage.saveToken(result.token);
        //localStorage.setItem('Authorization', result.token);
        this.authenticationService.markLoginValid();
      } else {
        this.showProgressBar = false;
        this.errorMessage =  true;
      }
    }, errMsg=> {
      console.log('login error' , errMsg);
      this.showProgressBar = false;
      this.errorMessage =  true;
    });
  }

}
