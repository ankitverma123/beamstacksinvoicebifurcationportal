import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../../login/login.component';
import { AuthGuards } from '../../guards/auth-guards';
import { HomeLayoutComponent } from '../../layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from '../../layout/login-layout/login-layout.component';
import { DiscoveryDashboardComponent } from 'src/app/components/charts/discovery-dashboard/discovery-dashboard.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { InvoicesComponent } from '../../components/invoices/invoices.component';
import { LocalUsersComponent } from 'src/app/components/adminComponents/local-users/local-users.component';
import { WorkFlowStatesComponent } from 'src/app/components/adminComponents/work-flow-states/work-flow-states.component';
import { BankIcaManagementComponent } from 'src/app/components/adminComponents/bank-ica-management/bank-ica-management.component';
import { ConfigurationsComponent } from 'src/app/components/adminComponents/configurations/configurations.component';
import { CreateModifyUserComponent } from 'src/app/components/adminComponents/local-users/create-modify-user/create-modify-user.component';
const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuards],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'invoices',
        component: InvoicesComponent,
      }, {
        path: 'localUsers',
        component: LocalUsersComponent,
      }, {
        path: 'configurations',
        component: ConfigurationsComponent,
      }, {
        path: 'bankIcaManagement',
        component: BankIcaManagementComponent,
      }, {
        path: 'createModifyUser',
        component: CreateModifyUserComponent,
      }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
