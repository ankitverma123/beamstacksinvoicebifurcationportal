import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import 'hammerjs';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { MaterialModule } from './material/material.module';
import { AuthGuards } from './guards/auth-guards';
import { AuthenticationService } from './_services/auth/authentication.service';
import { AppRoutingModule } from './Routing/app-routing/app-routing.module';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';
import { LoginComponent } from './login/login.component';
import { TopToolBarComponent } from './fabric/top-tool-bar/top-tool-bar.component';
import { PageBarComponent } from './fabric/page-bar/page-bar.component';
import { ProgressBarComponent } from './fabric/progress-bar/progress-bar.component';
import { NgxUploaderModule } from 'ngx-uploader';
import { PageButtonsComponent } from './fabric/page-buttons/page-buttons.component';
import { ToolbarComponent } from './fabric/toolbar/toolbar.component';
import { DiscoveryDashboardComponent } from './components/charts/discovery-dashboard/discovery-dashboard.component';
import { QuillModule } from 'ngx-quill';
import { CookieService } from 'ngx-cookie-service';
import { WebSocketServiceService } from './_services/websocket/web-socket-service.service';
import { TokenStorage } from './guards/TokenStorage';
import { TabNavigationService } from './_services/common/tabNavigation.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FabricGridComponent } from './fabric/fabric-grid/fabric-grid.component';
import { InvoicesComponent } from './components/invoices/invoices.component';
import { FabricGridCompInclude } from './fabric/fabric-grid/ng-include';
import { FixedFooterComponent } from './fabric/fixed-footer/fixed-footer.component';
import { FabricAgGridComponent } from './fabric/fabric-ag-grid/fabric-ag-grid.component';
import { ButtonRendererComponent } from './fabric/fabric-ag-grid/button-randerer.component';
import { IconbuttonRendererComponent } from './fabric/fabric-ag-grid/iconbutton-renderer.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpCalIInterceptor } from './http.interceptor';
import { InvoiceImportComponent } from './components/invoices/invoice-import/invoice-import.component';
import { RejectInvoiceComponent } from './components/invoices/reject-invoice/reject-invoice.component';
import { ApproveInvoiceComponent } from './components/invoices/approve-invoice/approve-invoice.component';
import { LocalUsersComponent } from './components/adminComponents/local-users/local-users.component';
import { WorkFlowStatesComponent } from './components/adminComponents/work-flow-states/work-flow-states.component';
import { BankIcaManagementComponent } from './components/adminComponents/bank-ica-management/bank-ica-management.component';
import { ConfigurationsComponent } from './components/adminComponents/configurations/configurations.component';
import { BankManagerComponent } from './components/adminComponents/bank-ica-management/bank-manager/bank-manager.component';
import { IcaManagerComponent } from './components/adminComponents/bank-ica-management/ica-manager/ica-manager.component';
import { CreateModifyUserComponent } from './components/adminComponents/local-users/create-modify-user/create-modify-user.component';
import { ViewInvoiceComponent } from './components/invoices/view-invoice/view-invoice.component';

// Function for setting the default restangular configuration
export function RestangularConfigFactory(RestangularProvider) {
  //localStorage.setItem('Authorization', null);
  RestangularProvider.setBaseUrl('http://localhost:8080');
  RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
    return {
      headers: Object.assign({}, headers, { 'Authorization': new TokenStorage().getToken() })
    }
  });
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    TopToolBarComponent,
    PageBarComponent,
    ProgressBarComponent,
    PageButtonsComponent,
    ToolbarComponent,
    DiscoveryDashboardComponent,
    DashboardComponent,
    FabricGridComponent,
    InvoicesComponent,
    FabricGridCompInclude,
    FixedFooterComponent,
    FabricAgGridComponent,
    ButtonRendererComponent,
    IconbuttonRendererComponent,
    InvoiceImportComponent,
    RejectInvoiceComponent,
    ApproveInvoiceComponent,
    LocalUsersComponent,
    WorkFlowStatesComponent,
    BankIcaManagementComponent,
    ConfigurationsComponent,
    BankManagerComponent,
    IcaManagerComponent,
    CreateModifyUserComponent,
    ViewInvoiceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    MaterialModule,
    AppRoutingModule,
    NgxUploaderModule,
    QuillModule,
    AgGridModule.withComponents([]),
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpCalIInterceptor, multi: true },
    TokenStorage,
    AuthGuards,
    AuthenticationService,
    CookieService,
    WebSocketServiceService,
    TabNavigationService],
  bootstrap: [AppComponent],
  entryComponents: [InvoiceImportComponent,
    RejectInvoiceComponent,
    ApproveInvoiceComponent,
    BankManagerComponent,
    IcaManagerComponent,
    ViewInvoiceComponent]
})
export class AppModule { }
