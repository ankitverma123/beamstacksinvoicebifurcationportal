import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NavbarService } from '../Navigation/navbar.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthenticationService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(private router: Router, private httpClient: HttpClient) {}

  login(loginFormData: any): any {
    return this.httpClient.post(`/auth`, loginFormData , {}).map((res: Response) => res).catch(this.handleError);
  }

  markLoginValid() {
    this.loggedIn.next(true);
    this.router.navigate(['']);
  }

  logout() {
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
