import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private restangular: Restangular) { }

  groupWithNoOwner(requestingFor){
    return this.restangular.one('/rest/group/noOwner/'+requestingFor).get().map((res:Response)=>res).catch(this.handleError);
  }

  groupWithActiveOwner(requestingFor){
    return this.restangular.one('/rest/group/activeOwner/'+requestingFor).get().map((res:Response)=>res).catch(this.handleError);
  }

  groupWithInactiveOwner(requestingFor){
    return this.restangular.one('/rest/group/inActiveOwner/'+requestingFor).get().map((res:Response)=>res).catch(this.handleError);
  }

  groupWithNoMember(requestingFor){
    return this.restangular.one('/rest/group/noMember/'+requestingFor).get().map((res:Response)=>res).catch(this.handleError);
  }

  dormantGroupCount() {
    return this.restangular.one('/rest/group/dormantGroupCount').get().map((res:Response)=>res).catch(this.handleError);
  }

  groupCount() {
    return this.restangular.one('/rest/group/groupcount').get().map((res: Response) => res).catch(this.handleError);
  }

  groupsInScopeCount() {
    return this.restangular.one('/rest/group/groupsInScope').get().map((res: Response) => res).catch(this.handleError);
  }

  groupsToBeRemediatedCount() {
    return this.restangular.one('/rest/group/groupsToBeRemediatedCount').get().map((res: Response) => res).catch(this.handleError);
  }

  getSurveyStateData() {
    return this.restangular.one('/rest/survey/surveyStateData').get().map((res: Response) => res).catch(this.handleError);
  }

  getSurveySubmissionSplit() {
    return this.restangular.one('/rest/survey/surveySubmissionSplit').get().map((res: Response) => res).catch(this.handleError);
  }

  getGroupsToBeRemediatedNoDoubleCounting() {
    return this.restangular.one('/rest/survey/groupsToBeRemediated').get().map((res: Response) => res).catch(this.handleError);
  }

  getRemindersCountData() {
    return this.restangular.one('/rest/survey/reminderCount').get().map((res: Response) => res).catch(this.handleError);
  }

  getWeeksData() {
    return this.restangular.one('/rest/survey/weeksData').get().map((res: Response) => res).catch(this.handleError);
  }

  getFirstDiscoveryData() {
    return this.restangular.one('/rest/group/getFirstDiscoveryData').get().map((res: Response) => res).catch(this.handleError);
  }

  getRecentDiscoveryData() {
    return this.restangular.one('/rest/group/getRecentDiscoveryData').get().map((res: Response) => res).catch(this.handleError);
  }

  getReminderDates() {
    return this.restangular.one('/rest/survey/getReminderDates').get().map((res: Response) => res).catch(this.handleError);
  }

  getCampaignStartDate() {
    return this.restangular.one('/rest/survey/getCampaignStartDate').get().map((res: Response) => res).catch(this.handleError);
  }

  getCount(requestedFor) {
    return this.restangular.one('/rest/groupdashboard/?requestFor='+requestedFor).get().map((res: Response) => res).catch(this.handleError);
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
