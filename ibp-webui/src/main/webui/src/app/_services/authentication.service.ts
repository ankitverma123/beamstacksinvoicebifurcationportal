import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private restangular: Restangular, private httpClient: HttpClient) { }

  login(loginFormData): Observable <any>{
    return this.restangular.one('/auth').customPOST(loginFormData,"",null,{}).map((res:Response)=>res).catch(this.handleError);
  }

  testCall():Observable<any>{
    return this.restangular.one('/rest/role/').get().map((res:Response)=>res).catch(this.handleError);;
  }

  logout(){

  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
