import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { HttpClient } from '@angular/common/http';
import { TokenStorage } from '../../guards/TokenStorage';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient, private tokenStorage: TokenStorage) { }

  getCall(url): Observable <any>{
    return this.httpClient.get(url).map((res: Response) => res).catch(this.handleError);
  }

  postCall(url, payload): Observable <any>{
    return this.httpClient.post(url, payload , {}).map((res: Response) => res).catch(this.handleError);
  }

  loggedInUserFirstName() {
    return this.getTokenDetails()['firstname']
  }

  loggedInUserLastName() {
    return this.getTokenDetails()['lastname']
  }

  loggedInUserUserName() {
    return this.getTokenDetails()['username']
  }

  loggedInUserEmail() {
    return this.getTokenDetails()['email']
  }

  loggedInUserRole() {
    let roles = []
    for(var x=0; x<this.getTokenDetails()['authorities'].length; x++){
      roles.push(this.getTokenDetails()['authorities'][0]['authority']);
    }
    return roles;
  }

  getTokenDetails() {
    const ca = this.tokenStorage.getToken();
    var base64Url = ca.split('.')[1];
    var decodedValue = JSON.parse(window.atob(base64Url));
    return decodedValue;
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
