import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Injectable()

export class TabNavigationService {

  constructor() {

  }
  public nextIndex = 0;
  public currentIndex = 0;
  public isDirty: boolean;
  public form: FormGroup;
  public isSubmitted = false;

  public validate() {
    // console.log('Submitted: '+ this.isSubmitted);
    // console.log(this.form);
    if (this.currentIndex !== this.nextIndex && !this.form.dirty) {
      // console.log('NO popup');
      this.currentIndex = this.nextIndex;

    } else if (this.currentIndex !== this.nextIndex && this.isSubmitted) {
      this.currentIndex = this.nextIndex;
    }
    else if (this.currentIndex !== this.nextIndex && this.form.dirty && !this.isSubmitted) {
      let response = confirm('Discard Changes?');

      if (response) {
        this.form.reset();
        this.currentIndex = this.nextIndex;
      }

    }
  }
}
